CREATE OR REPLACE VIEW v_product_stock AS 
select 
  `store_product`.`store_product_id` AS `transactionId`, 
  `product`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  `store_product`.`owning_store_id` AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  concat(
    'Opening Balance of', ' ', `product`.`product_name`
  ) AS `transactionDescription`, 
  `store_product`.`store_quantity` AS `dr_quantity`, 
  '0' AS `cr_quantity`, 
  (
    `product`.`product_unitprice` * `store_product`.`store_quantity`
  ) AS `dr_amount`, 
  '0' AS `cr_amount`, 
  `store_product`.`created` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Income' AS `transactionCategory`, 
  'Product Opening Stock' AS `transactionClassification`, 
  'store_product' AS `transactionTable`, 
  'product' AS `referenceTable` 
from 
  (
    (
      `store_product` 
      join `product` on(
        (
          (
            `product`.`product_id` = `store_product`.`product_id`
          ) 
          and (`product`.`product_deleted` = 0)
        )
      )
    ) 
    join `store` on(
      (
        `store`.`store_id` = `store_product`.`owning_store_id`
      )
    )
  ) 
union all 
select 
  `order_supplier`.`order_supplier_id` AS `transactionId`, 
  `product`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  `orders`.`store_id` AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  concat(
    'Purchase of', ' ', `product`.`product_name`
  ) AS `transactionDescription`, 
  (
    `order_supplier`.`quantity_received` * `order_supplier`.`pack_size`
  ) AS `dr_quantity`, 
  '0' AS `cr_quantity`, 
  `order_supplier`.`total_amount` AS `dr_amount`, 
  '0' AS `cr_amount`, 
  `orders`.`supplier_invoice_date` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Income' AS `transactionCategory`, 
  'Supplier Purchases' AS `transactionClassification`, 
  'order_item' AS `transactionTable`, 
  'orders' AS `referenceTable` 
from 
  (
    (
      (
        (
          `order_item` 
          join `order_supplier`
        ) 
        join `product`
      ) 
      join `orders`
    ) 
    join `store` on(
      (
        `store`.`store_id` = `orders`.`store_id`
      )
    )
  ) 
where 
  (
    (
      `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
    ) 
    and (
      `order_item`.`product_id` = `product`.`product_id`
    ) 
    and (
      `orders`.`order_id` = `order_item`.`order_id`
    ) 
    and (`product`.`product_deleted` = 0) 
    and (`orders`.`supplier_id` > 0) 
    and (`orders`.`is_store` < 2) 
    and (
      `orders`.`order_approval_status` = 7
    ) 
    and (`product`.`product_id` <> 0)
  ) 
union all 
select 
  `product_purchase`.`purchase_id` AS `transactionId`, 
  `product_purchase`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  `product_purchase`.`store_id` AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  `product_purchase`.`purchase_description` AS `transactionDescription`, 
  (
    `product_purchase`.`purchase_quantity` * `product_purchase`.`purchase_pack_size`
  ) AS `dr_quantity`, 
  '0' AS `cr_quantity`, 
  (
    `product`.`product_unitprice` * (
      `product_purchase`.`purchase_quantity` * `product_purchase`.`purchase_pack_size`
    )
  ) AS `dr_amount`, 
  '0' AS `cr_amount`, 
  `product_purchase`.`purchase_date` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Income' AS `transactionCategory`, 
  'Product Addition' AS `transactionClassification`, 
  'product_purchase' AS `transactionTable`, 
  'product' AS `referenceTable` 
from 
  (
    (
      `product_purchase` 
      join `product`
    ) 
    join `store` on(
      (
        `store`.`store_id` = `product_purchase`.`store_id`
      )
    )
  ) 
where 
  (
    (
      `product`.`product_id` = `product_purchase`.`product_id`
    ) 
    and (`product`.`product_deleted` = 0)
  ) 
union all 
select 
  `product_deductions_stock`.`product_deductions_stock_id` AS `transactionId`, 
  `product_deductions_stock`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  `product_deductions_stock`.`store_id` AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  `product_deductions_stock`.`deduction_description` AS `transactionDescription`, 
  '0' AS `dr_quantity`, 
  (
    `product_deductions_stock`.`product_deductions_stock_quantity` * `product_deductions_stock`.`product_deductions_stock_pack_size`
  ) AS `cr_quantity`, 
  '0' AS `dr_amount`, 
  (
    `product`.`product_unitprice` * (
      `product_deductions_stock`.`product_deductions_stock_quantity` * `product_deductions_stock`.`product_deductions_stock_pack_size`
    )
  ) AS `cr_amount`, 
  `product_deductions_stock`.`product_deductions_stock_date` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Expense' AS `transactionCategory`, 
  'Product Deductions' AS `transactionClassification`, 
  'product_deductions_stock' AS `transactionTable`, 
  'product' AS `referenceTable` 
from 
  (
    (
      `product_deductions_stock` 
      join `product`
    ) 
    join `store` on(
      (
        `store`.`store_id` = `product_deductions_stock`.`store_id`
      )
    )
  ) 
where 
  (
    (
      `product`.`product_id` = `product_deductions_stock`.`product_id`
    ) 
    and (`product`.`product_deleted` = 0)
  ) 
union all 
select 
  `order_supplier`.`order_supplier_id` AS `transactionId`, 
  `product`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  `orders`.`store_id` AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  concat(
    'Credit note of', ' ', `product`.`product_name`
  ) AS `transactionDescription`, 
  '0' AS `dr_quantity`, 
  (
    `order_supplier`.`quantity_received` * `order_supplier`.`pack_size`
  ) AS `cr_quantity`, 
  '0' AS `dr_amount`, 
  `order_supplier`.`total_amount` AS `cr_amount`, 
  `orders`.`supplier_invoice_date` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Expense' AS `transactionCategory`, 
  'Supplier Credit Note' AS `transactionClassification`, 
  'order_item' AS `transactionTable`, 
  'orders' AS `referenceTable` 
from 
  (
    (
      (
        (
          `order_item` 
          join `order_supplier`
        ) 
        join `product`
      ) 
      join `orders`
    ) 
    join `store` on(
      (
        `store`.`store_id` = `orders`.`store_id`
      )
    )
  ) 
where 
  (
    (
      `order_item`.`order_item_id` = `order_supplier`.`order_item_id`
    ) 
    and (
      `order_item`.`product_id` = `product`.`product_id`
    ) 
    and (
      `orders`.`order_id` = `order_item`.`order_id`
    ) 
    and (`product`.`product_deleted` = 0) 
    and (`orders`.`supplier_id` > 0) 
    and (
      `orders`.`order_approval_status` = 7
    ) 
    and (`orders`.`is_store` = 3)
  ) 
union all 
select 
  `visit_charge`.`visit_charge_id` AS `transactionId`, 
  `product`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  6 AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  concat(
    'Product Sale', ' ', `product`.`product_name`
  ) AS `transactionDescription`, 
  '0' AS `dr_quantity`, 
  `visit_charge`.`visit_charge_units` AS `cr_quantity`, 
  '0' AS `dr_amount`, 
  (
    `visit_charge`.`visit_charge_units` * `visit_charge`.`buying_price`
  ) AS `cr_amount`, 
  `visit_charge`.`date` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Expense' AS `transactionCategory`, 
  'Drug Sales' AS `transactionClassification`, 
  'visit_charge' AS `transactionTable`, 
  'product' AS `referenceTable` 
from 
  (
    (
      `visit_charge` 
      join `product`
    ) 
    join `store` on(
      (`store`.`store_id` = 6)
    )
  ) 
where 
  (
    (`visit_charge`.`charged` = 1) 
    and (
      `visit_charge`.`visit_charge_delete` = 0
    ) 
    and (
      `product`.`product_id` = `visit_charge`.`product_id`
    ) 
    and (`product`.`product_deleted` = 0)
  ) 
union all 
select 
  `product_deductions`.`product_deductions_id` AS `transactionId`, 
  `product`.`product_id` AS `product_id`, 
  `product`.`category_id` AS `category_id`, 
  `store`.`store_id` AS `store_id`, 
  '' AS `receiving_store`, 
  `product`.`product_name` AS `product_name`, 
  `store`.`store_name` AS `store_name`, 
  concat(
    'Product Transfered', ' ', `product`.`product_name`
  ) AS `transactionDescription`, 
  '0' AS `dr_quantity`, 
  (
    `product_deductions`.`quantity_given` * `product_deductions`.`pack_size`
  ) AS `cr_quantity`, 
  '0' AS `dr_amount`, 
  (
    `product`.`product_unitprice` * (
      `product_deductions`.`quantity_given` * `product_deductions`.`pack_size`
    )
  ) AS `cr_amount`, 
  `product_deductions`.`search_date` AS `transactionDate`, 
  `product`.`product_status` AS `status`, 
  `product`.`product_deleted` AS `product_deleted`, 
  'Expense' AS `transactionCategory`, 
  'Drug Transfer' AS `transactionClassification`, 
  'product_deductions' AS `transactionTable`, 
  'product' AS `referenceTable` 
from 
  (
    (
      (
        `product_deductions` 
        join `product`
      ) 
      join `orders`
    ) 
    join `store` on(
      (
        `orders`.`store_id` = `store`.`store_id`
      )
    )
  ) 
where 
  (
    (
      `product_deductions`.`order_id` = `orders`.`order_id`
    ) 
    and (
      `product_deductions`.`product_id` = `product`.`product_id`
    ) 
    and (`product`.`product_deleted` = 0) 
    and (`orders`.`supplier_id` > 0) 
    and (`orders`.`is_store` = 2) 
    and (
      `orders`.`order_approval_status` = 7
    )
  )
