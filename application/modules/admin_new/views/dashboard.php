<?php



$date_today = date('Y-m-d');
 $community_where ='patients.patient_type = 0 AND patients.patient_delete = 0 AND  DATE(patients.patient_date) = "'.$date_today.'"';
$community_table = 'patients';
$total_number_new = $this->reception_model->count_items($community_table, $community_where);




$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
$community_table = 'patients,visit';
$total_outpatients = $this->reception_model->count_items($community_table, $community_where);


$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 7)  AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND  DATE(visit.visit_date) <> "'.$date_today.'"';
$community_table = 'patients,visit';
$total_inpatients = $this->reception_model->count_items($community_table, $community_where);


$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 0 OR visit.close_card = 7)  AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0';
$community_table = 'patients,visit';
$new_inpatients = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 1 AND (visit.close_card = 2)  AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND DATE(visit.visit_time_out) = "'.$date_today.'"';
$community_table = 'patients,visit';
$discharges = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.revisit = 0 AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
$community_table = 'patients,visit';
$total_newisits = $this->reception_model->count_items($community_table, $community_where);



$date_today = date('Y-m-d');
$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND visit.inpatient = 0 AND visit.revisit = 1 AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
$community_table = 'patients,visit';
$total_revisits = $this->reception_model->count_items($community_table, $community_where);

?>
<div class="row">
	<!-- <div class="col-md-12"> -->
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-primary">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">TODAY'S REGISTRATIONS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_number_new?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-success">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">TODAY'S OUTPATIENTS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_outpatients?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-info">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">ACTIVE INPATIENTS</h5>
								<div class="info">
									<strong class="amount"><?php echo $new_inpatients?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
		</div>

		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-warning">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">TODAY'S DISCHARGES</h5>
								<div class="info">
									<strong class="amount"><?php echo $discharges?></strong>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">
			<section class="card mb-4">
				<div class="card-body bg-danger">
					<div class="widget-summary">
						<!-- <div class="widget-summary-col widget-summary-col-icon">
							<div class="summary-icon">
								<i class="fa fa-calendar"></i>
							</div>
						</div> -->
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title">NEW VISITS VS REVISITS</h5>
								<div class="info">
									<strong class="amount"><?php echo $total_newisits.' - '.$total_revisits?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-md-2">

			<section class="card mb-4">
				<div class="card-body bg-default">
					<div class="widget-summary">
						
						<div class="widget-summary-col">
							<div class="summary">
								<h5 class="title" style="color: #fff">APPOINTMENTS</h5>
								<div class="info">
									<strong class="amount" style="color: #fff"><?php echo '0';?></strong>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</section>
			
		</div>
		


	<!-- </div> -->
</div>

<div class="row" style="margin-top:20px;">

	<div class="col-md-4">	

				<h5>SUMMARIES ON PHARMACY SOLD ITEMS</h5>	
				<br>
					<div class="panel-body">
					<?php

		$date_tomorrow = date('Y-m-d');
		$visit_date = date('jS M Y',strtotime($date_tomorrow));
						$pharmacist_results = $this->reception_model->get_all_pharmacists();
		$counting =0;
		$date_from =$date_tomorrow;
		$date_to =$date_tomorrow;
		$pharm_results ='';
		if($pharmacist_results->num_rows() > 0)
		{
		$count = $full = $percentage = $daily = $hourly = 0;

			$pharm_results .=  
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th style="padding:5px;">PERSONNEL</th>
						   <th style="padding:5px;">PATIENTS</th>
						  <th style="padding:5px;">CASH</th>
						  <th style="padding:5px;">INSURANCE</th>
						</tr>
					</thead>
					<tbody>
				';
			$result_pharm = $pharmacist_results->result();
			$grand_total = 0;
			$patients_total = 0;
			$grand_total_insurance =0;
			
			foreach($result_pharm as $res)
			{
				$personnel_id = $res->personnel_id;
				$personnel_onames = $res->personnel_onames;
				$personnel_fname = $res->personnel_fname;
				$personnel_type_id = $res->personnel_type_id;
				$count++;
				
				//get service total
				$total = $this->reception_model->get_total_pharm_collected($personnel_id, $date_from, $date_to);
				$patients = $this->reception_model->get_total_pharm_patients($personnel_id, $date_from, $date_to);
				$insurance = $this->reception_model->get_total_pharm_collected_insurance($personnel_id, $date_from, $date_to);

				
				$grand_total += $total;
				$grand_total_insurance += $insurance;
				$patients_total += $patients;
				

				$pharm_results.= '
					<tr>
						<td style="padding:5px;">'.$count.'</td>
						<td >'.strtoupper($personnel_fname).' '.strtoupper($personnel_onames).'</td>
						<td style="padding:5px;">'.$patients.'</td>
						<td style="text-align:center;padding:5px;">'.number_format($total, 2).'</td>
						<td style="text-align:center;padding:5px;">'.number_format($insurance, 2).'</td>
						
					</tr>
				';
			}
			
			$pharm_results.= 
			'
				
					<tr>
						<td colspan="2">TOTAL</td>
						<td colspan="1">'.$patients_total.'</td>
						<td style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total, 2).'</span></td>
						<td style="text-align:center;border-top:2px solid #000;"><span class="bold">'.number_format($grand_total_insurance, 2).'</span></td>
					</tr>
				</tbody>
			</table>
			';
		}


					?>


					<table  class="table table-hover table-bordered ">
						<thead>
							<tr>
								<th style="padding:5px;">VISIT TYPE</th>
								<th style="padding:5px;">OUTPATIENTS</th>
								<th style="padding:5px;">INPATIENTS</th>
								<th style="padding:5px;">TOTAL PATIENTS</th>
								
							</tr>
						</thead>
						</tbody> 
						  	<?php echo $pharm_results?>
					  	</tbody>
					</table>
				</div>
			</div>


			<div class="col-md-4">	

				<h5>PATIENTS SEEN/REGISTERED TODAY (PER VISIT TYPE)</h5>	
				<br>
					<div class="panel-body">
					<?php
						$date_today = date('Y-m-d');
						$visit_types_rs = $this->reception_model->get_visit_types();
						$visit_results = '';
					
						if($visit_types_rs->num_rows() > 0)
						{
							foreach ($visit_types_rs->result() as $key => $value) {
								# code...

								$visit_type_name = $value->visit_type_name;
								$visit_type_id = $value->visit_type_id;


								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 0 AND patients.patient_delete = 0 AND visit.visit_date = "'.$date_today.'"';
								$total_outpatient_patients = $this->reception_model->count_items($table,$where);
							




								$table = 'visit,patients';
								$where = 'visit.visit_delete = 0 AND patients.patient_id = visit.patient_id AND visit.visit_type = '.$visit_type_id.' AND visit.inpatient = 1 AND patients.patient_delete = 0  AND visit.visit_date = "'.$date_today.'"';
								$total_inpatient_patients = $this->reception_model->count_items($table,$where);



								$total_visits = $total_outpatient_patients + $total_inpatient_patients;
								// calculate amounts paid
							
								$visit_results .='<tr>
												  		<td style="text-align:left;"> '.strtoupper($visit_type_name).'  </td>
												  		<td style="text-align:center;"> '.$total_outpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_inpatient_patients.'</td>
												  		<td style="text-align:center;"> '.$total_visits.'</td>
												  	</tr>';
								
					


							}

							
						}


					?>


					<table  class="table table-hover table-bordered ">
						<thead>
							<tr>
								<th style="padding:5px;">VISIT TYPE</th>
								<th style="padding:5px;">OUTPATIENTS</th>
								<th style="padding:5px;">INPATIENTS</th>
								<th style="padding:5px;">TOTAL PATIENTS</th>
								
							</tr>
						</thead>
						</tbody> 
						  	<?php echo $visit_results?>
					  	</tbody>
					</table>
				</div>
			</div>
		
			<div class="col-md-4">
				<h5>NEW VISITS AND REVISITS PER DEPARTMENT</h5>	
				<br>
				<div class="panel-body">
                <?php
                   
                     $this->db->where('report_item = 1');
                    $query = $this->db->get('departments');
                    $result = '';
                    
                    //if users exist display them
                    if ($query->num_rows() > 0)
                    {
                        $count = $page;
                        
                        $result .= 
                            '
                                <table class="table table-hover table-bordered ">
                                  <thead>
                                    <tr>
                                      <th>#</th>
                                      <th>DEPARTMENT</th>
                                      <th>NEW VISITS</th>
                                      <th>REVISITS</th>
                                      <th>TOTAL</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                            ';
                        
                        foreach ($query->result() as $row)
                        {
                            
                            $department_name = $row->department_name;
                            $department_id = $row->department_id;
                            
                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 0 AND patients.patient_type = 0 AND DATE(visit.visit_date) = "'.$date_today.'"';
                            $community_table = 'visit,patients';
                            $total_number_new = $this->reception_model->count_items($community_table, $community_where);

                            $community_where ='visit.department_id = '.$department_id.' AND patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 1 AND patients.patient_type = 0 AND DATE(visit.visit_date) = "'.$date_today.'"';
                            $community_table = 'visit,patients';
                            $total_number_old = $this->reception_model->count_items($community_table, $community_where);
                            $total = $total_number_new + $total_number_old;
                            $count++;
                            $result .= 
                                '
                                    <tr>
                                        <td>'.$count.'</td>
                                        <td>'.strtoupper($department_name).'</td>
                                        <td>'.$total_number_new.'</td>
                                        <td>'.$total_number_old.'</td>
                                        <td>'.$total.'</td>
                                        
                                    </tr> 
                                ';
                        }
                        
                        $result .= 
                        '
                                      </tbody>
                                    </table>
                        ';
                    }
                    
                    else
                    {
                        $result .= "There are no departments";
                    }
                    
                    echo $result;
                    ?>

                    <?php

                    $date_today = date('Y-m-d');
					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 1 AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
					$community_table = 'patients,visit';
					$total_men = $this->reception_model->count_items($community_table, $community_where);


					  $date_today = date('Y-m-d');
					$community_where ='patients.patient_type = 0 AND visit.patient_id = patients.patient_id AND patients.gender_id = 2 AND visit.visit_delete = 0 AND visit.personnel_id > 0 AND patients.patient_delete = 0 AND  DATE(visit.visit_date) = "'.$date_today.'"';
					$community_table = 'patients,visit';
					$total_female = $this->reception_model->count_items($community_table, $community_where);
                    ?>
                    <h5>VISITS PER GENDER</h5>	
        			<br>
                	<table class="table table-hover table-bordered ">
                  
                        <tr>
                          <th>MALE</th>
                          <td><?php echo $total_men?></td>
                          <th>FEMALE</th>
                          <td><?php echo $total_female?></td>
                        </tr>
                     
                 
                     </table>


                </div>
				
         
				
			</div>
			

</div>


<script type="text/javascript">
	
	
</script>
					
					
					