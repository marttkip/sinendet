<?php

// $where = 'patients.patient_delete = 0 ';
// $table = 'patients';
// $total_patient = $this->reception_model->count_items($table, $where);


// $where = 'patients.patient_delete = 0 AND category_id = 1';
// $table = 'patients';
// $new_patient = $this->reception_model->count_items($table, $where);

// $where = 'patients.patient_delete = 0 AND category_id = 3';
// $table = 'patients';
// $uncategorized_patient = $this->reception_model->count_items($table, $where);

// $where = 'patients.patient_delete = 0 AND category_id = 2';
// $table = 'patients';
// $uhdc_patient = $this->reception_model->count_items($table, $where);




$total_charts = '';
for ($i = 6; $i >= 0; $i--) {
    $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$i months"));
    $months_explode = explode('-', $months);
    $year = $months_explode[0];
    $month = $months_explode[1];
    $last_visit = date('M Y',strtotime($months));
    $where = 'patients.patient_delete = 0 AND YEAR(patient_date) = '.$year.' AND MONTH(patient_date) = "'.$month.'" AND patients.patient_id IN (SELECT visit.patient_id FROM visit)';
	$table = 'patients';
	$month_patients = $this->reception_model->count_items($table, $where);
	$total_charts .= '["'.$last_visit.'", '.$month_patients.'],';

}



$where = 'patients.patient_delete = 0 AND gender_id = 1  AND patients.patient_id IN (SELECT visit.patient_id FROM visit)';
	$table = 'patients';
	$male_patients = $this->reception_model->count_items($table, $where);

$where = 'patients.patient_delete = 0 AND gender_id = 2  AND patients.patient_id IN (SELECT visit.patient_id FROM visit)';

$table = 'patients';
$female_patients = $this->reception_model->count_items($table, $where);
$total_gender = '{
                        label: "Male",
                        data: [
                            [1, '.$male_patients.']
                        ],
                        color: "#0088cc"
                    }, {
                        label: "Female",
                        data: [
                            [1, '.$female_patients.']
                        ],
                        color: "#734ba9"
                    }';




$total_visits = '';
for ($k = 6; $k >= 0; $k--) {
    $months = date("Y-m", strtotime( date( 'Y-m-d' )." -$k months"));
    $months_explode = explode('-', $months);
    $year = $months_explode[0];
    $month = $months_explode[1];
    $last_visit = date('M Y',strtotime($months));
   
    $community_where ='patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 0 AND patients.patient_type = 0 AND  YEAR(visit.visit_date) = '.$year.' AND MONTH(visit.visit_date) = "'.$month.'"';
    $community_table = 'visit,patients';
    $total_number_new = $this->reception_model->count_items($community_table, $community_where);

    $community_where ='patients.patient_id = visit.patient_id AND visit.personnel_id > 0 AND visit.revisit = 1 AND patients.patient_type = 0 AND  YEAR(visit.visit_date) = '.$year.' AND MONTH(visit.visit_date) = "'.$month.'"';
    $community_table = 'visit,patients';
    $total_number_old = $this->reception_model->count_items($community_table, $community_where);

	$total_visits .= '{
	                        y: "'.$last_visit.'",
	                        a: '.$total_number_new.',
	                        b: '.$total_number_old.'
	                    },';

}

// var_dump($total_visits); die();
?>
<!-- start: page -->
<div class="row" style="margin-top:20px;">
	<div class="col-md-6 col-lg-12 col-xl-12">
		<section class="panel">
			<div class="panel-body">
				<div class="row">
					<div class="col-lg-6">						

						<section class="card">
				            <header class="card-header">
				               	<h4 class="card-title">Patient Turnover</h4>
				                <p class="card-subtitle">Displays the patients turnover in the hospital for the past six months according to registrations</p>
				            </header>
				            <div class="card-body">

				                <!-- Flot: Bars -->
				                <div class="chart chart-md" id="flotBars"></div>
				                <script type="text/javascript">
				                    var flotBarsData = [<?php echo $total_charts?>
				                    ];

				                    // See: js/examples/examples.charts.js for more settings.
				                </script>

				            </div>
				        </section>
					</div>
					<div class="col-lg-6">
						<section class="card">
					            <header class="card-header">				               

					                <h4 class="card-title">Patients Gender</h4>
					                <p class="card-subtitle">Comparison of total patients according to gender</p>
					            </header>
					            <div class="card-body">

					                <!-- Flot: Pie -->
					                <div class="chart chart-md" id="flotPie"></div>
					                <script type="text/javascript">
					                    var flotPieData = [<?php echo $total_gender?>];

					                    // See: js/examples/examples.charts.js for more settings.
					                </script>

					            </div>
					        </section>
                   
					</div>
				</div>
			</div>
		</section>
	</div>
	
</div>

<div class="row" >
	<div class="col-md-6 col-lg-12 col-xl-12">
		<section class="panel">
			<div class="panel-body">
				<div class="row">
					
					<div class="col-lg-12">
						<section class="card">
				            <header class="card-header">
				                <h4 class="card-title">New Visits VS Re-visits Comparisons</h4>
				                <p class="card-subtitle">Comparison between new visits and revisit for the past six months</p>
				            </header>
				            <div class="card-body">

				                <!-- Morris: Bar -->
				                <div class="chart chart-md" id="morrisBar"></div>
				                <script type="text/javascript">
				                    var morrisBarData = [<?php echo $total_visits;?>];

				                    // See: js/examples/examples.charts.js for more settings.
				                </script>

				            </div>
				        </section>
					</div>
				</div>
			</div>
		</section>
	</div>
	
</div>