<?php
$contacts = $this->site_model->get_contacts();
$logo = $contacts['logo'];
?>
<?php
$success = $this->session->userdata('success_message');

if(!empty($success))
{
    echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
    $this->session->unset_userdata('success_message');
}

$error = $this->session->userdata('error_message');

if(!empty($error))
{
    echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
    $this->session->unset_userdata('error_message');
}
?>
<!-- <section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Leave Balance</h2>
    </header>
    <div class="panel-body">


    
</div>
    </div> -->
</section>
<?php 

$personnel_id = $this->session->userdata('personnel_id');
$is_admin = $this->admin_model->check_if_admin($personnel_id);

if($personnel_id == 1 OR $personnel_id == 2 OR $personnel_id = 0)
{
    
// echo $this->load->view('hr/leave/leave_summaries', '', true);
}

?>

<?php //echo $this->load->view('calendar', '', TRUE);?>

<section class="panel">
	<header class="panel-heading">
    	<h2 class="panel-title">Profile Details</h2>
    </header>
	<div class="panel-body">
	<?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}
			
		?>
		<div class="row">
            <div class="col-sm-12 col-md-4 col-lg-3">
                <img src="<?php echo base_url();?>assets/logo/<?php echo $logo;?>">
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <h5><strong>Profile Details</strong></h5>
                <div class="form-group">
                    <label class="col-lg-4 control-label"><strong> Personnel Name:</strong> </label>
                    
                    <div class="col-lg-8">
                        <?php echo $this->session->userdata('first_name');?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="col-lg-4 control-label"> <strong>Username:</strong> </label>
                    
                    <div class="col-lg-8">
                        <?php echo $this->session->userdata('username');?>
                    </div>
                    
                </div>
             <!--    <div class="form-group">
                    <label class="col-lg-4 control-label"><strong>Branch Name: </strong></label>
                    
                    <div class="col-lg-8">
                        <?php //echo $this->session->userdata('branch_name');?>
                    </div>
                    
                </div> -->
             <!--    <div class="form-group">
                    <label class="col-lg-4 control-label"><strong> Branch Code:</strong> </label>
                    
                    <div class="col-lg-8">
                        <?php //echo $this->session->userdata('branch_code');?>
                    </div>
                    
                </div> -->
            </div>
            <div class="col-sm-6 col-md-4 col-lg-6">
                <h5><strong>Change Password</strong></h5>
                <form enctype="multipart/form-data" action="<?php echo base_url();?>change-password"  id = "change_password" method="post">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="slideshow_name"> <strong>Current Password </strong></label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="current_password" placeholder="Current Password" >
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="slideshow_name"><strong>New Password</strong></label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="new_password" placeholder="New Password" >
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="slideshow_name"><strong>Confirm New Password</strong></label>
                                    <div class="col-md-8">
                                        <input type="password" class="form-control" name="confirm_new_password" placeholder="Confirm new password" >
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                    <div class="form-group center-align">
                                        <button type="submit" class="btn btn-sm btn-success" name="submit" >Change Password</button>
                                    </div>
                                
                            </div>
                                
                        </div>
                        
                    </div>
                </form>
            </div>
		</div>
	</div>
</section>
	
<div class="row">
    <div class="col-md-12">
        <section class="card">
            <header class="card-header">
                <div class="card-actions">
                    <a href="#" class="card-action card-action-toggle" data-card-toggle></a>
                    <a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
                </div>

                <h2 class="card-title">Bar Chart</h2>
                <p class="card-subtitle">A bar chart is a way of summarizing a set of categorical data.</p>
            </header>
            <div class="card-body">

                <!-- Morris: Bar -->
                <div class="chart chart-md" id="morrisBar"></div>
                <script type="text/javascript">

                    
                    // See: js/examples/examples.charts.js for more settings.

                </script>

            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    /*
    Morris: Bar
    */
    var morrisBarData = [{
                        y: '2004',
                        a: 10,
                        b: 30
                    }, {
                        y: '2005',
                        a: 100,
                        b: 25
                    }, {
                        y: '2006',
                        a: 60,
                        b: 25
                    }, {
                        y: '2007',
                        a: 75,
                        b: 35
                    }, {
                        y: '2008',
                        a: 90,
                        b: 20
                    }, {
                        y: '2009',
                        a: 75,
                        b: 15
                    }, {
                        y: '2010',
                        a: 50,
                        b: 10
                    }, {
                        y: '2011',
                        a: 75,
                        b: 25
                    }, {
                        y: '2012',
                        a: 30,
                        b: 10
                    }, {
                        y: '2017',
                        a: 75,
                        b: 5
                    }, {
                        y: '2017',
                        a: 60,
                        b: 8
                    }];

    if( $('#morrisBar').get(0) ) {
        // alert(morrisBarData);
        Morris.Bar({
            resize: true,
            element: 'morrisBar',
            data: [{
                        y: '2004',
                        a: 10,
                        b: 30
                    }, {
                        y: '2005',
                        a: 100,
                        b: 25
                    }, {
                        y: '2006',
                        a: 60,
                        b: 25
                    }, {
                        y: '2007',
                        a: 75,
                        b: 35
                    }, {
                        y: '2008',
                        a: 90,
                        b: 20
                    }, {
                        y: '2009',
                        a: 75,
                        b: 15
                    }, {
                        y: '2010',
                        a: 50,
                        b: 10
                    }, {
                        y: '2011',
                        a: 75,
                        b: 25
                    }, {
                        y: '2012',
                        a: 30,
                        b: 10
                    }, {
                        y: '2017',
                        a: 75,
                        b: 5
                    }, {
                        y: '2017',
                        a: 60,
                        b: 8
                    }],
            xkey: 'y',
            ykeys: ['a', 'b'],
            labels: ['Series A', 'Series B'],
            hideHover: true,
            barColors: ['#0088cc', '#2baab1']
        });
    }

</script>