<?php

//patient details
$visit_type = $patient['visit_type'];
$patient_type = $patient['patient_type'];
$patient_othernames = $patient['patient_othernames'];
$patient_surname = $patient['patient_surname'];
$patient_surname = $patient['patient_surname'];
$patient_number = $patient['patient_number'];
$visit_id = $patient['visit_id'];
$gender = $patient['gender'];

$today = date('jS F Y H:i a',strtotime(date("Y:m:d h:i:s")));
$visit_date = date('jS F Y',strtotime($this->accounts_model->get_visit_date($visit_id)));

//doctor
$doctor = $this->accounts_model->get_att_doctor($visit_id);

//served by
$served_by = $this->accounts_model->get_personnel($this->session->userdata('personnel_id'));
$credit_note_amount = $this->accounts_model->get_sum_credit_notes($visit_id);
$debit_note_amount = $this->accounts_model->get_sum_debit_notes($visit_id);
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $contacts['company_name'];?> | Receipt</title>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="all"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="all"/>
        <style type="text/css">
        body{font-family:"Courier New", Courier, monospace}
        .receipt_spacing{letter-spacing:0px; font-size: 12px;}
        .center-align{margin:0 auto; text-align:center;}
        
        .receipt_bottom_border{border-bottom: #888888 medium solid;}
        .row .col-md-12 table {
            border:solid #000 !important;
            border-width:1px 0 0 1px !important;
            font-size:10px;
        }
        .row .col-md-12 th, .row .col-md-12 td {
            border:solid #000 !important;
            border-width:0 1px 1px 0 !important;
        }
        .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td
        {
             padding: 2px;
        }
        
        .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
        .title-img{float:left; padding-left:30px;}
        img.logo{max-height:70px; margin:0 auto;}
    </style>
    </head>
    <body class="receipt_spacing">
        <div class="row">
            <div class="col-xs-12">
                <img src="<?php echo base_url().'assets/logo/'.$contacts['logo'];?>" alt="<?php echo $contacts['company_name'];?>" class="img-responsive logo"/>
            </div>
        </div>
   <div class="row">
            <div class="col-md-12 center-align receipt_bottom_border">
                <strong>
                    <?php echo $contacts['company_name'];?><br/>
                    P.O. Box <?php echo $contacts['address'];?> <?php echo $contacts['post_code'];?>, <?php echo $contacts['city'];?><br/>
                    E-Mail:<?php echo $contacts['email'];?>.<br> Tel : <?php echo $contacts['phone'];?><br/>
                    <?php echo $contacts['location'];?><br/>
                </strong>
            </div>
        </div>

        
        <div class="row" >
            <div class="col-md-12 center-align">
                <strong>RECEIPT</strong>
            </div>
        </div>
        
        <!-- Patient Details -->
        <div class="row receipt_bottom_border" style="margin-bottom: 20px; font-size:10px; font-weight: bold;">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        
                        <div class="title-item">Patient Name:</div>
                        
                        <?php echo $patient_surname.' '.$patient_othernames; ?>
                    </div>
                </div>
                          </div>
            
            </div>
        
          <div class="row" style="margin-bottom: 20px; font-weight: bold;">
            <div class="col-md-12">
               <table class="table table-hover table-bordered table-striped col-md-12">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Services/Items</th>
                                    <th>Units</th>
                                    <th>Unit Cost (Ksh)</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $service_rs = $this->accounts_model->get_patient_visit_charge($visit_id);
                    $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items($visit_id);
                    $total = 0;
                    $s=0;
                    
                    if(count($service_rs) > 0)
                    {
                        foreach($service_rs as $serv)
                        {
                            $service_id = $serv->service_id;
                            $service_name = $serv->service_name;
                            $visit_total = 0;
                            
                            if(count($item_invoiced_rs) > 0){
                              
                              foreach ($item_invoiced_rs as $key_items):
                                $service_id2 = $key_items->service_id;
                                $service_charge_id = $key_items->service_charge_id;
                                
                                if($service_id2 == $service_id)
                                {
                                    if($service_id == 4)
                                    {
                                        if($this->accounts_model->in_pres($service_charge_id, $visit_id))
                                        {
                                            $visit_charge_amount = $key_items->visit_charge_amount;
                                            $units = $key_items->visit_charge_units;
                    
                                            $visit_total += $visit_charge_amount * $units;
                                        }
                                    }
                                    
                                    else
                                    {
                                        $visit_charge_amount = $key_items->visit_charge_amount;
                                        $units = $key_items->visit_charge_units;
                
                                        $visit_total += $visit_charge_amount * $units;
                                    }
                                }
                              endforeach;
                            }
                            
                            // end of the payments

                            $total = $total + $visit_total;
                        }
                        // enterring the payment stuff
                        $payments_rs = $this->accounts_model->payments($visit_id);
                        $total_payments = 0;
                        $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                        
                        if(count($payments_rs) > 0){
                            $x = $s;
                            foreach ($payments_rs as $key_items):
                                
                                $payment_id = $key_items->payment_id;
                                $payment_method = $key_items->payment_method;
                                $amount_paid = $key_items->amount_paid;
                                $time = $key_items->time;
                                $payment_type = $key_items->payment_type;
                                $amount_paidd = number_format($amount_paid,2);
                                $payment_service_id = $key_items->payment_service_id;

                                if($payment_service_id > 0)
                                {
                                    $service_associate = $this->accounts_model->get_service_detail($payment_service_id);
                                }
                                else
                                {
                                    $service_associate = " ";
                                }
                            endforeach;
                        }
                        $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                    }
                    $total_amount = ($total + $debit_note_amount) - $credit_note_amount;
                    $payments_rs = $this->accounts_model->payments($visit_id);
                    $total_payments = 0;
                    
                    if(count($payments_rs) > 0)
                    {
                        $x=0;
                        
                        foreach ($payments_rs as $key_items):
                            $x++;
                            $payment_type = $key_items->payment_type;
                            $payment_status = $key_items->payment_status;
                            
                            if($payment_type == 1 && $payment_status == 1)
                            {
                                $payment_id = $key_items->payment_id;
                                $amount_paid = $key_items->amount_paid;
                            
                                $total_payments = $total_payments + $amount_paid;
                                
                                if($payment_id == $receipt_payment_id)
                                {
                                    $payment_method = $key_items->payment_method;
                                    $transaction_code = $key_items->transaction_code;
                                    $payment_service_id = $key_items->payment_service_id;
                                    $service_name = $payment_method.' '.$transaction_code;
                                    
                                    if(count($service_rs) > 0)
                                    {
                                        foreach($service_rs as $serv)
                                        {
                                            $service_id = $serv->service_id;
                                            if($payment_service_id == $service_id)
                                            {
                                                $service_name = $serv->service_name;
                                                break;
                                            }
                                        }
                                    }
                                                    
                                    //display DN & CN services
                                    if((count($payments_rs) > 0) && ($service_name == ''))
                                    {
                                        foreach ($payments_rs as $key_items):
                                            $payment_type = $key_items->payment_type;
                                            
                                            if(($payment_type == 2) || ($payment_type == 3))
                                            {
                                                $payment_service_id2 = $key_items->payment_service_id;
                                                
                                                if($payment_service_id2 == $payment_service_id)
                                                {
                                                    $service_name = $this->accounts_model->get_service_detail($payment_service_id);
                                                    break;
                                                }
                                            }
                                            
                                        endforeach;
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="2"><?php echo $service_name;?> <?php echo $payment_method;?> <?php echo $transaction_code;?></td>
                                        <td><?php echo number_format($amount_paid, 2);?></td>
                                    </tr>
                                    <?php
                                }
                            }


                        endforeach;
                        
                    }
                                    $total = 0;
                                    $item_invoiced_rs = $this->accounts_model->get_patient_visit_charge_items_tree($visit_id);
                                    $total_amount= 0; 
                                    $days = 0;
                                    if($item_invoiced_rs->num_rows() > 0)
                                    {
                                        foreach ($item_invoiced_rs->result() as  $value) {
                                            # code...
                                            $service_id= $value->service_id;
                                            $service_name = $value->service_name;

                                            echo"
                                                    <tr > 
                                                        <td colspan='9'><strong>".$service_name."</strong></td>
                                                    </tr> 
                                                ";

                                            $rs2 = $this->accounts_model->get_visit_procedure_charges_per_service($visit_id,$service_id); 

                                            // if($service_name == "Bed charge")
                                            // {
                                            //  $days = count($rs2);
                                            // }
                                            
                                            // var_dump($service_name); die();
                                            $sub_total= 0; 
                                            $personnel_query = $this->personnel_model->retrieve_personnel();
                                                
                                            if(count($rs2) >0){
                                                $count = 0;
                                                $visit_date_day = '';
                                                
                                                foreach ($rs2 as $key1):
                                                    $v_procedure_id = $key1->visit_charge_id;
                                                    $procedure_id = $key1->service_charge_id;
                                                    $date = $key1->date;
                                                    $time = $key1->time;
                                                    $visit_charge_timestamp = $key1->visit_charge_timestamp;
                                                    $visit_charge_amount = $key1->visit_charge_amount;
                                                    $units = $key1->visit_charge_units;
                                                    $procedure_name = $key1->service_charge_name;
                                                    $service_id = $key1->service_id;
                                                    $provider_id = $key1->provider_id;
                                                
                                                    
                                                    $visit_date = date('l d F Y',strtotime($date));
                                                    $visit_time = date('H:i A',strtotime($visit_charge_timestamp));
                                                    if($visit_date_day != $visit_date)
                                                    {
                                                        
                                                        $visit_date_day = $visit_date;
                                                    }
                                                    else
                                                    {
                                                        $visit_date_day = '';
                                                    }

                                                    

                                                    if($personnel_query->num_rows() > 0)
                                                    {
                                                        $personnel_result = $personnel_query->result();
                                                        
                                                        foreach($personnel_result as $adm)
                                                        {
                                                            $personnel_id = $adm->personnel_id;
                                                            
                                                            
                                                                if($personnel_id == $provider_id)
                                                                {
                                                                    $provider_id = ' [ Dr. '.$adm->personnel_fname.' '.$adm->personnel_lname.']';

                                                                    $procedure_name = $procedure_name.$provider_id;
                                                                }
                                                            
                                                            

                                                            
                                                            
                                                            
                                                        }

                                                    }
                                                    
                                                    else
                                                    {
                                                        $provider_id = '';
                                                        
                                                    }
                                                    if($procedure_name == 'Bed Charges')
                                                    {
                                                        $days++;
                                                    }




                                                    $count++;
                                                    if($procedure_id != 7769)
                                                    {
                                                        $sub_total= $sub_total +($units * $visit_charge_amount);
                                                        echo"
                                                                <tr> 
                                                                    <td >".$count."</td>
                                                                    <td >".$procedure_name."</td>
                                                                    <td align='right'>".$units."</td>
                                                                    <td align='right'>".number_format($visit_charge_amount,2)."</td>
                                                                    <td align='right'>".number_format($units * $visit_charge_amount,2)."</td>
                                                                    
                                                                </tr>   
                                                        ";
                                                    
                                                    $visit_date_day = $visit_date;
                                                    }
                                                    endforeach;
                                                    

                                            }
                                                  $total_amount = $total_amount + $sub_total;

                                        }
                                    }
                                    
                                    $total_services = count($services_billed);
                                    if($total_services > 0)
                                    {
                                        for($t = 0; $t < $total_services; $t++)
                                        {
                                            $s++;
                                            $debit_note_pesa  = 0;
                                            $credit_note_pesa = 0;
                                            
                                            $payment_service_name = $services_billed[$t]['payment_service_name'];
                                            $payment_service_id = $services_billed[$t]['payment_service_id'];
                                            
                                            $debit_note_pesa = $this->accounts_model->total_debit_note_per_service($payment_service_id, $visit_id);
                                            
                                            $credit_note_pesa = $this->accounts_model->total_credit_note_per_service($payment_service_id, $visit_id);
                                            //get service name
                                            $service_name = $payment_service_name;
                                            if($debit_note_pesa > 0)
                                            {
                                                ?>
                                                <tr>
                                                    <td><?php echo $s;?></td>
                                                    <td><?php echo $service_name;?></td>
                                                    <td>Debit notes</td>
                                                    <td>1</td>
                                                    <td><?php echo number_format($debit_note_pesa,2);?></td>
                                                    <td><?php echo number_format($debit_note_pesa,2);?></td>
                                                </tr>
                                                <?php
                                            }
                                            
                                            if($credit_note_pesa > 0)
                                            {
                                                ?>
                                                <tr>
                                                    <td><?php echo $s;?></td>
                                                    <td><?php echo $service_name;?></td>
                                                    <td>Credit notes</td>
                                                    <td>1</td>
                                                    <td>(<?php echo number_format($credit_note_pesa,2);?>)</td>
                                                    <td>(<?php echo number_format($credit_note_pesa,2);?>)</td>
                                                </tr>
                                                <?php
                                            }
                                            $total_amount = ($total_amount + $debit_note_pesa) - $credit_note_pesa;
                                        }
                                    }
                                    
                                    if($days > 0)
                                    {
                                        $days = $days -1;
                                    }
                                  
                                      ?>
                                     
                          <tr class="receipt_bottom_border"  style="margin-bottom: 20px; font-weight: bold;">
                        <td colspan="2">Total Paid</td>
                        <td><?php echo number_format($total_payments, 2);?></td>
                        </tr>
                        <tr class="receipt_bottom_border" style="margin-bottom: 20px; font-weight: bold;">
                        <td colspan="2">Balance</td>
                        <td><?php echo number_format($total_amount - $total_payments, 2);?></td>
                      </tr>
                                   
                                    
                                </tbody>
                              </table>
            </div>
        </div>
        
        <div class="" style="font-style:italic; font-size:20px; font-weight: bold;">
            <div style="float:left; margin:0 10px 0 10px;">
                Served by: <?php echo $served_by; ?>
            </div>
            <div style="float:right; margin:0 10px 0 10px font-size:20px; font-weight: bold;">
                <?php echo $today; ?> Thank you
            </div>
        </div>
    </body>
    
</html>