<?php
// get visit details


$visit_rs = $this->reception_model->get_visit_details($visit_id);

foreach ($visit_rs as $key) {
	# code...

	$time_end=$key->time_end;
	$time_start=$key->time_start;
	$visit_date=$key->visit_date;
	$personnel_id=$key->personnel_id;
	$room_id=$key->room_id;
	$patient_id=$key->patient_id;
	// $scheme_name=$key->scheme_name;
	$insurance_description=$key->insurance_description;
	$patient_insurance_number=$key->patient_insurance_number;
	$insurance_limit=$key->insurance_limit;
	$principal_member=$key->principal_member;
}

if($chart_type == 1)
{
	$child_items = '';
	$adult_items = 'checked';
}
else
{
	$child_items = 'checked';
	$adult_items = '';
}
// var_dump($patient_id);die();
?>
<div class="col-print-6" >
	<div class="col-md-12">
		<div class="form-group">
			<label class="col-lg-4 control-label">Type of chart? </label>
	        <div class="col-lg-4">
	            <div class="radio">
	                <label>
	                    <input id="optionsRadios1" type="radio" name="chart_type" <?php echo $child_items;?> id="chart_type_one" onclick="get_chart_type(0)" value="0">
	                    Child
	                </label>
	            </div>
	        </div>
	        
	        <div class="col-lg-4">
	            <div class="radio">
	                <label>
	                    <input id="optionsRadios1" type="radio" name="chart_type" <?php echo $adult_items;?> id="chart_type_two" onclick="get_chart_type(1)"  value="1">
	                    Adult
	                </label>
	            </div>
	        </div>
		</div>

	</div>
	<div id="page_item"></div>
	
	
	
</div>
<div class="col-print-6" >
	<div class="col-md-12" style="border-bottom: 1px solid grey; margin-bottom: 2px;">
		<div class="col-md-12">
			<?php 

			// echo '<div class="alert alert-info">
			// 	1.  Click on  <a  class="btn btn-xs btn-success" ><i class="fa fa-arrow-right"></i> Bill </a> to bill the patient on that particular day and <a  class="btn btn-danger btn-xs  " ><i class="fa fa-arrow-left"></i> unbill </a> to remove from that day\'s bill.<br>
			// 	2. Ensure you have clicked on the button <a  class="btn btn-danger btn-xs  " > Send to accounts </a> for the accounts office to get the bill. Below the page
			// 	</div>';

				?>
			
		</div>
		<div class="col-md-12">
			<div class="col-md-6">
				<h4 class="center-align"><i id="toothnumber"></i></h4>
			</div>
			<div class="col-md-6">
				<a class="btn btn-sm btn-warning pull-right" href="<?php echo site_url().'queue'?>" > <i class="fa fa-arrow-left"></i> Back to queue </a>
			</div>
			
		</div>
		
				
	</div>
	<div class="col-md-12" style="height: 300px; overflow-y:  scroll;">
		
		<!-- <hr> -->
		<div class="col-md-12" >
			<div class="col-print-3">  
				<div id="dentine-list-div"></div>	
				
			</div>
			<div class="col-print-5">   

			    <div class="form-group">                    
			        <div class="col-md-10">
			        	<div id="procedure-search-div" style="display: block">
			        		<input type="text" class="form-control" name="q" id="q" placeholder="procedures" onkeyup="procedure_lists(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" >
			        	</div>
			        	<div id="service_charge-div" style="display: none">
			        		<div class="col-md-12" style="margin-bottom: 10px;">
			        			<input type="text" class="form-control" name="service_charge_name" id="service_charge_name" placeholder="Procedure Name" >
			        		</div>
			        		<div class="col-md-12" >
			        			<a onclick="add_service_charge(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> Add Procedure</a>
			        		</div>
			        	</div>
			            
			        </div>
			        <div class="col-md-2">
			        	<div id="new-button-div" style="display: block">
				        	<a onclick="add_new_procedure(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" class="btn btn-xs btn-info"><i class="fa fa-plus"></i></a>
				        </div>
				        <div id="add-button-div" style="display: none">
				        	<a onclick="back_procedure_list(<?php echo $visit_id;?>,<?php echo $patient_id;?>)" class="btn btn-xs btn-warning"><i class="fa fa-arrow-left"></i></a>
				        </div>
			        </div>
			    </div>
			    <div class="col-print-12" id="procedure-div" style="display: block;">
			    	<div id="procedure-lists"></div>
			    </div>
			</div>
			<div class="col-print-4">
				<div id="teeth-marker"></div>
				<div class="col-md-12" >
					<div class="center-align">
						<a class="btn btn-sm btn-success  " onclick="pass_tooth()"> Update Dental</a>	
					</div>
									 	
				</div>
			</div>
		</div>


	</div>
</div>