<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// require_once "./application/modules/auth/controllers/auth.php";
class Cloud  extends MX_Controller
{	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('cloud_model');
	}
	
	public function save_cloud_data()
	{
		$json = file_get_contents('php://input');
	   	$response = $this->cloud_model->save_visit_data($json);

		echo json_encode($response);
	}
	public function sync_up_petty_cash()
	{
		$json = file_get_contents('php://input');
	   	$response = $this->cloud_model->save_petty_cash_data($json);

		echo json_encode($response);

	}
	
	public function cron_sync_up()
	{
		//get all unsynced visits
		$unsynced_visits = $this->cloud_model->get_unsynced_visits();
		
		$patients = array();
		
		if($unsynced_visits->num_rows() > 0)
		{
			//delete all unsynced visits
			$this->db->where('sync_status', 0);
			if($this->db->delete('sync'))
			{
				foreach($unsynced_visits->result() as $res)
				{
					$sync_data = $res->sync_data;
					$sync_table_name = $res->sync_table_name;
					$branch_code = $res->branch_code;
					array_push($patients[$sync_table_name], $value);
					
					/*$response = $this->sync_model->syn_up_on_closing_visit($visit_id);
		
					if($response)
					{
					}*/
				}
				$patients['branch_code'] = $branch_code;
			}
		}
		
		//sync data
		$response = $this->cloud_model->send_unsynced_visits($patients);
		var_dump($response);
		if($response)
		{
		}
		
		else
		{
		}
	}
	public function backup()
    {
        // Load the DB utility class
        $this->load->dbutil();
        $date = date('Y-m-d H:i:s');
        $prefs = array(
            'ignore'        => array('table'),                     // List of tables to omit from the backup
            'format'        => 'txt',                       // gzip, zip, txt
            'filename'      => $date.'_backup.sql',              // File name - NEEDED ONLY WITH ZIP FILES
            'newline'       => "\n"                         // Newline character used in backup file
        );

        // Backup your entire database and assign it to a variable
        $backup = $this->dbutil->backup($prefs);

        // Load the file helper and write the file to your server
        $date = date('YmdHis');

        $this->load->helper('file');
        write_file('C://Users/ACCN-SERVER/Google Drive/'.$date.'.sql', $backup);

        // Load the download helper and send the file to your desktop
        // $this->load->helper('download');
        // force_download('mybackup.gz', $backup);



    }
}
?>