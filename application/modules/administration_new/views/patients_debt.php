<!-- search -->
<?php echo $this->load->view('search/patient_debt', '', TRUE);?>
<!-- end search -->
 
<div class="row">
    <div class="col-md-12">

        <section class="panel panel-featured panel-featured-info">
            <header class="panel-heading">
            	 <h2 class="panel-title"><?php echo $title;?></h2>
            </header>             

          <!-- Widget content -->
                <div class="panel-body">
<?php
		$result = '';
		$search = $this->session->userdata('all_debt_search');
		if(!empty($search))
		{
			echo '<a href="'.site_url().'administration/reports/close_debt_search" class="btn btn-sm btn-warning">Close Search</a>';
		}

		$error = $this->session->userdata('error_message');
		$success = $this->session->userdata('success_message');
		
		if(!empty($error))
		{
			echo '<div class="alert alert-danger">'.$error.'</div>';
			$this->session->unset_userdata('error_message');
		}
		
		if(!empty($success))
		{
			echo '<div class="alert alert-success">'.$success.'</div>';
			$this->session->unset_userdata('success_message');
		}
		
		$method_rs = $this->accounts_model->get_payment_methods();

		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
				'
					<table class="table table-hover table-bordered table-striped table-responsive col-md-12">
					  <thead>
						<tr>
						  <th>#</th>
						  <th>Patient</th>
						  <th>Phone</th>
						  <th>Last Visit</th>						 
						  <th>Invoiced</th>
						  <th>Payments</th>
						  <th>Waivers</th>
						  <th>Cash Bal</th>
						  <th>Insurance Bal</th>
						  <th>Balance</th>
						  <th>Last Reminder</th>
						  <th colspan="2"></th>
						</tr>
					  </thead>
					  <tbody>
			';
			
			$personnel_query = $this->personnel_model->get_all_personnel();
			
			foreach ($query->result() as $row)
			{
				$total_invoiced = 0;
				$last_visit = date('jS M Y',strtotime($row->last_visit));
				
				$patient_othernames = $row->patient_othernames;
				$patient_surname = $row->patient_surname;
				$patient_phone1 = $row->patient_phone1;
				$total_invoice_amount = $row->total_invoice_amount;
				$total_paid_amount = $row->total_paid_amount;
				$total_waived_amount = $row->total_waived_amount;
				$total_balance = $row->total_balance;
				$patient_id = $row->patient_id;
				$sent_hits = $row->sent_hits;
				$last_reminder = $row->last_reminder;

				if(!empty($last_reminder))
				{
					$last_reminder = date('jS M Y',strtotime($row->last_reminder));
				}
				else
				{
					$last_reminder = '';
				}
				$cash_balance = $this->accounts_model->get_cash_balance($patient_id);
				$insurance_balance = $this->accounts_model->get_insurance_balance($patient_id);

				$count++;
				
					$result .= 
						'
							<tr>
								<td>'.$count.'</td>
								<td>'.$patient_surname.' '.$patient_othernames.'</td>
								<td>'.$patient_phone1.'</td>
								<td>'.$last_visit.'</td>
								<td>'.number_format($total_invoice_amount,2).'</td>
								<td>'.number_format($total_paid_amount,2).'</td>
								<td>'.number_format($total_waived_amount,2).'</td>
								<td>'.number_format($cash_balance,2).'</td>
								<td>'.number_format($insurance_balance,2).'</td>
								<td>'.number_format($total_balance,2).'</td>
								<td>'.$last_reminder.'</td>
								<td><a  class="btn btn-xs btn-success" href="'.site_url().'send-patient-message/'.$patient_id.'/'.$total_balance.'/'.$page.'" onclick="return confirm(\' Are you sure you want to send '.$patient_surname.' '.$patient_othernames.' his/her balance ?  \')"> send message </a>
								
							</tr> 
					';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no visits";
		}
		
		echo $result;
?>
          </div>
          
          <div class="widget-foot">
                                
				<?php if(isset($links)){echo $links;}?>
            
                <div class="clearfix"></div> 
            
            </div>
        
		</section>
    </div>
  </div>
  <script type="text/javascript">
  	
  	function check_payment_type(visit_id){
   		
   		var payment_type_id = $('#payment_type_id'+visit_id).val();

   		// alert(payment_type_id);
	    var myTarget1 = document.getElementById("cheque_div"+visit_id);

	    var myTarget2 = document.getElementById("mpesa_div"+visit_id);

	    var myTarget3 = document.getElementById("insuarance_div"+visit_id);

	    if(payment_type_id == 1)
	    {
	      // this is a check
	     
	      myTarget1.style.display = 'block';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	    }
	    else if(payment_type_id == 2)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	    }
	    else if(payment_type_id == 3)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'block';
	    }
	    else if(payment_type_id == 4)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'none';
	    }
	    else if(payment_type_id == 5)
	    {
	      myTarget1.style.display = 'none';
	      myTarget2.style.display = 'block';
	      myTarget3.style.display = 'none';
	    }
	    else
	    {
	      myTarget2.style.display = 'none';
	      myTarget3.style.display = 'block';  
	    }

  	WWW}
  </script>