<?php

// presenting complaint 
$table ='presenting_complaint';
$where ='presenting_complaint_delete = 0 AND visit_id ='.$visit_id;
$select = '*';

$complaint_rs = $this->dental_model->get_items_from_table($table,$where,$select);
// var_dump($complaint_rs); die();
$complaint_div ='';
if($complaint_rs->num_rows() > 0)
{
	foreach ($complaint_rs->result() as $key) {
		# code...

		// $presenting_complaint_id=$key->presenting_complaint_id;
		$presenting_complaint=$key->presenting_complaint;
	}

	$complaint_div ='<textarea class="form-control" class="cleditor" rows="5" name="presenting_complaint'.$visit_id.'" id="presenting_complaint'.$visit_id.'">'.$presenting_complaint.'</textarea>';
}
else
{
	$complaint_div ='<textarea class="form-control" class="cleditor" rows="5" name="presenting_complaint'.$visit_id.'" id="presenting_complaint'.$visit_id.'">'.set_value('presenting_complaint').'</textarea>';
}


// end of presenting complaint


// presenting complaint 
$table ='dental_history';
$where ='visit_id ='.$visit_id;
$select = '*';

$dental_history = $this->dental_model->get_items_from_table($table,$where,$select);
$past_dental_history ='';
if($dental_history->num_rows() > 0)
{
	foreach ($dental_history->result() as $key_dental) {
		# code...

		$last_dental_visit=$key_dental->last_dental_visit;
		$xray_done=$key_dental->xray_done;
		$procedures_done=$key_dental->procedures_done;
		$sensitivity_to=$key_dental->sensitivity_to;
		$straightened_teeth=$key_dental->straightened_teeth;
		$brushing_teeth=$key_dental->brushing_teeth;
		$dental_floss=$key_dental->dental_floss;
		$bleeding_gums=$key_dental->bleeding_gums;
		$gum_treatment=$key_dental->gum_treatment;
		$dental_fear=$key_dental->dental_fear;
		$teeth_appearance=$key_dental->teeth_appearance;

		$concern=$key_dental->concern;
		$discomfort_status=$key_dental->discomfort_status;
		$discomfort=$key_dental->discomfort;





	}

	$past_dental_history ='

					  		 <div class="form-group" style="display:none;">
			                    <label class="col-md-4 control-label">What concerns you most ? </label>
			                    
			                    <div class="col-md-8">
			                       <textarea class="form-control" name="concern'.$visit_id.'" id="concern'.$visit_id.'" >'.$concern.' </textarea>
			                    </div>
			                </div>
			                 <div class="form-group">
			                    <label class="col-md-4 control-label">Are you having discomfort at this time ? </label>
			                    
			                    <div class="col-md-8">
			                        <div class="col-md-6">
		                                <div class="radio">
		                                    <label>
		                                        <input id="optionsRadios1" type="radio" checked value="1" name="discomfort_status'.$visit_id.'" id="discomfort_status'.$visit_id.'">
		                                        Yes
		                                    </label>
		                                </div>
		                            </div>
		                            <div class="col-md-6">
		                                <div class="radio">
		                                    <label>
		                                        <input id="optionsRadios2" type="radio" value="2" name="discomfort_status'.$visit_id.'" id="discomfort_status'.$visit_id.'">
		                                        No
		                                    </label>
		                                </div>
		                            </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <label class="col-md-4 control-label">What is the discomfort ? </label>
			                    
			                    <div class="col-md-8">
			                        <textarea class="form-control cleditor" name="discomfort'.$visit_id.'" id="discomfort'.$visit_id.'" >'.$discomfort.' </textarea>
			                    </div>
			                </div>
			               



						  ';
}
else
{

$last_dental_visit= set_value('last_dental_visit');
	$xray_done= set_value('xray_done');

	$procedures_done= set_value('procedures_done');
	$sensitivity_to= set_value('sensitivity_to');

	$straightened_teeth= set_value('straightened_teeth');
	$brushing_teeth= set_value('brushing_teeth');

	$dental_floss= set_value('dental_floss');
	$bleeding_gums= set_value('bleeding_gums');

	$gum_treatment= set_value('gum_treatment');
	$dental_fear= set_value('dental_fear');
	$teeth_appearance= set_value('teeth_appearance');
	$discomfort= set_value('discomfort');
	$discomfort_status= set_value('discomfort_status');
	$concern= set_value('concern');
	$past_dental_history ='
 							<div class="form-group" style="display:none;">
			                    <label class="col-md-4 control-label">What concerns you most ? </label>
			                    
			                    <div class="col-md-8">
			                       <textarea class="form-control" name="concern'.$visit_id.'" id="concern'.$visit_id.'" >'.$concern.' </textarea>
			                    </div>
			                </div>
			                 <div class="form-group">
			                    <label class="col-md-4 control-label">Are you having discomfort at this time ? </label>
			                    
			                    <div class="col-md-8">
			                        <div class="col-md-6">
		                                <div class="radio">
		                                    <label>
		                                        <input id="optionsRadios1" type="radio" checked value="1" name="discomfort_status'.$visit_id.'">
		                                        Yes
		                                    </label>
		                                </div>
		                            </div>
		                            <div class="col-md-6">
		                                <div class="radio">
		                                    <label>
		                                        <input id="optionsRadios2" type="radio" value="2" name="discomfort_status'.$visit_id.'">
		                                        No
		                                    </label>
		                                </div>
		                            </div>
			                    </div>
			                </div>
			                <div class="form-group">
			                    <label class="col-md-4 control-label">What is the discomfort ? </label>
			                    
			                    <div class="col-md-8">
			                        <textarea class="form-control cleditor"  name="discomfort'.$visit_id.'" id="discomfort'.$visit_id.'" >'.$discomfort.' </textarea>
			                    </div>
			                </div>


						  ';
}


// end of presenting complaint



// presenting complaint 
$table ='shaw_history';
$where ='patient_id ='.$patient_id;
$select = '*';

$medical_rs_rs = $this->dental_model->get_items_from_table_shaw($table,$where,$select);
$medical_div ='';
if($medical_rs_rs->num_rows() > 0)
{
	foreach ($medical_rs_rs->result() as $key_medical) {
		# code...

		
        $sna=$key_medical->sna;
        $snb=$key_medical->snb;
        $air=$key_medical->air;
        $ufouw=$key_medical->ufouw;
        $ufour=$key_medical->ufour;
		$ufourr=$key_medical->ufourr;
		$usix=$key_medical->usix;
		$usixx=$key_medical->usixx;
		$lfour=$key_medical->lfour;
		$lfourr=$key_medical->lfourr;
		$lsix=$key_medical->lsix;
		$lsixx=$key_medical->lsixx;
		$ufouw=$key_medical->ufouw;
		$ufourm=$key_medical->ufourm;
		$usixm=$key_medical->usixm;
		$lsixm=$key_medical->lsixm;
		$lfourm=$key_medical->lfourm;
		$none=$key_medical->none;
		$physicians_care=$key_medical->physicians_care;
		$physician_name=$key_medical->physician_name;
		$physician_phone=$key_medical->physician_phone;
		$current_mediction=$key_medical->current_mediction;
		$allergic_reaction_causes=$key_medical->allergic_reaction_causes;
		$pregnant=$key_medical->pregnant;
		$nursing=$key_medical->nursing;
		$cheif=$key_medical->cheif;
        $pay=$key_medical->pay;
		$amount=$key_medical->amount;
		$photos=$key_medical->photos;
		$plan=$key_medical->plan;
		$method=$key_medical->method;
		$lower=$key_medical->lower;
		$upper=$key_medical->upper;
		$habits=$key_medical->habits;
		$check=$key_medical->check;
		$jet=$key_medical->jet;
		$angle_name=$key_medical->angle_name;
		$over=$key_medical->over;
		$face=$key_medical->face;
		$bites=$key_medical->bites;
		$cross=$key_medical->cross;
		$sulcus=$key_medical->sulcus;
		$mid=$key_medical->mid;
		$anterior=$key_medical->anterior;
		$lateral=$key_medical->lateral;
		$asprin=$key_medical->asprin;
		$erythromycin=$key_medical->erythromycin;
		$sedatives=$key_medical->sedatives;
		$barbiturates=$key_medical->barbiturates;
		$metals=$key_medical->metals;
		$sulpha_drugs=$key_medical->sulpha_drugs;
		$codeine=$key_medical->codeine;
		$latex=$key_medical->latex;
		$tetracycline=$key_medical->tetracycline;
		$dental_anesthetics=$key_medical->dental_anesthetics;
		$penicillin=$key_medical->penicillin;
		$liver_disease=$key_medical->liver_disease;
		$diabetes=$key_medical->diabetes;
		$hemophilia=$key_medical->hemophilia;
		$asthma=$key_medical->asthma;
		$glaucoma=$key_medical->glaucoma;
		$migraines=$key_medical->migraines;
		$chemotherapy=$key_medical->chemotherapy;
		$colitis=$key_medical->colitis;
		$lupus=$key_medical->lupus;
		$hepatitis=$key_medical->hepatitis;
		$seizures=$key_medical->seizures;
		$hiv=$key_medical->hiv;
		$chest_conditions=$key_medical->chest_conditions;
		$heart_conditions=$key_medical->heart_conditions;
		$kidney_problems=$key_medical->kidney_problems;
		$anemia=$key_medical->anemia;
		$artificial_bones=$key_medical->artificial_bones;
		$radiation_treatment=$key_medical->radiation_treatment;
		$high_blood_pressure=$key_medical->high_blood_pressure;
		$medical_conditions=$key_medical->medical_conditions;
		$control_pills=$key_medical->control_pills;



	}

	if($physicians_care == 1)
	{
		$physicians_care = 'checked';
		$physicians_care_default = '';
	}
	else
	{
		$physicians_care = '';
		$physicians_care_default = 'checked';
	}

	if($asprin == 1)
	{
		$asprin = 'checked';
		$asprin_default = '';
	}
	else
	{
		$asprin = '';
		$asprin_default = 'checked';
	}
	if($liver_disease == 1)
	{
		$liver_disease = 'checked';
		$liver_disease_default = '';
	}
	else
	{
		$liver_disease_default = 'checked';
		$liver_disease = '';
	}

	if($diabetes == 1)
	{
		$diabetes = 'checked';
		$diabetes_default = '';
	}
	else
	{
		$diabetes_default = 'checked';
		$diabetes = '';
	}
	if($hemophilia == 1)
	{
		$hemophilia = 'checked';
		$hemophilia_default = '';
	}
	else
	{
		$hemophilia_default = 'checked';
		$hemophilia = '';
	}

	if($asthma == 1)
	{
		$asthma = 'checked';
		$asthma_default = '';
	}
	else
	{
		$asthma_default = 'checked';
		$asthma = '';
	}

	if($glaucoma == 1)
	{
		$glaucoma = 'checked';
		$glaucoma_default = '';
	}
	else
	{
		$glaucoma_default = 'checked';
		$glaucoma = '';
	}

	if($migraines == 1)
	{
		$migraines = 'checked';
		$migraines_default = '';
	}
	else
	{
		$migraines_default = 'checked';
		$migraines = '';
	}

	if($chemotherapy == 1)
	{
		$chemotherapy = 'checked';
		$chemotherapy_default = '';
	}
	else
	{
		$chemotherapy_default = 'checked';
		$chemotherapy = '';
	}


	if($colitis == 1)
	{
		$colitis = 'checked';
		$colitis_default = '';
	}
	else
	{
		$colitis_default = 'checked';
		$colitis = '';
	}

	if($lupus == 1)
	{
		$lupus = 'checked';
		$lupus_default = '';
	}
	else
	{
		$lupus_default = 'checked';
		$lupus = '';
	}



	if($hepatitis == 1)
	{
		$hepatitis = 'checked';
		$hepatitis_default = '';
	}
	else
	{
		$hepatitis_default = 'checked';
		$hepatitis = '';
	}


	if($seizures == 1)
	{
		$seizures = 'checked';
		$seizures_default = '';
	}
	else
	{
		$seizures_default = 'checked';
		$seizures = '';
	}


	if($hiv == 1)
	{
		$hiv = 'checked';
		$hiv_default = '';
	}
	else
	{
		$hiv_default = 'checked';
		$hiv = '';
	}

	if($chest_conditions == 1)
	{
		$chest_conditions = 'checked';
		$chest_conditions_default = '';
	}
	else
	{
		$chest_conditions_default = 'checked';
		$chest_conditions = '';
	}

	if($heart_conditions == 1)
	{
		$heart_conditions = 'checked';
		$heart_conditions_default = '';
	}
	else
	{
		$heart_conditions_default = 'checked';
		$heart_conditions = '';
	}

	if($kidney_problems == 1)
	{
		$kidney_problems = 'checked';
		$kidney_problems_default = '';
	}
	else
	{
		$kidney_problems_default = 'checked';
		$kidney_problems = '';
	}

	if($anemia == 1)
	{
		$anemia = 'checked';
		$anemia_default = '';
	}
	else
	{
		$anemia_default = 'checked';
		$anemia = '';
	}

	if($artificial_bones == 1)
	{
		$artificial_bones = 'checked';
		$artificial_bones_default = '';
	}
	else
	{
		$artificial_bones_default = 'checked';
		$artificial_bones = '';
	}

	if($radiation_treatment == 1)
	{
		$radiation_treatment = 'checked';
		$radiation_treatment_default = '';
	}
	else
	{
		$radiation_treatment_default = 'checked';
		$radiation_treatment = '';
	}

	if($high_blood_pressure == 1)
	{
		$high_blood_pressure = 'checked';
		$high_blood_pressure_default = '';
	}
	else
	{
		$high_blood_pressure_default = 'checked';
		$high_blood_pressure = '';
	}

	if($control_pills == 1)
	{
		$control_pills = 'checked';
		$control_pills_default = '';
	}
	else
	{
		$control_pills_default = 'checked';
		$control_pills = '';
	}

	if($pregnant == 1)
	{
		$pregnant = 'checked';
		$pregnant_default = '';
	}
	else
	{
		$pregnant_default = 'checked';
		$pregnant = '';
	}

	if($nursing == 1)
	{
		$nursing = 'checked';
		$nursing_default = '';
	}
	else
	{
		$nursing_default = 'checked';
		$nursing = '';
	}


	if($erythromycin == 1)
	{
		$erythromycin = 'checked';
		$erythromycin_default = '';
	}
	else
	{
		$erythromycin_default = 'checked';
		$erythromycin = '';
	}
	if($sedatives == 1)
	{
		$sedatives = 'checked';
		$sedatives_default = '';
	}
	else
	{
		$sedatives_default = 'checked';
		$sedatives = '';
	}

	if($barbiturates == 1)
	{
		$barbiturates = 'checked';
		$barbiturates_default = '';
	}
	else
	{
		$barbiturates_default = 'checked';
		$barbiturates = '';
	}

	if($metals == 1)
	{
		$metals = 'checked';
		$metals_default = '';
	}
	else
	{
		$metals_default = 'checked';
		$metals = '';
	}

	if($sulpha_drugs == 1)
	{
		$sulpha_drugs = 'checked';
		$sulpha_drugs_default = '';
	}
	else
	{
		$sulpha_drugs_default = 'checked';
		$sulpha_drugs = '';
	}


	if($codeine == 1)
	{
		$codeine = 'checked';
		$codeine_default = '';
	}
	else
	{
		$codeine_default = 'checked';
		$codeine = '';
	}


	if($latex == 1)
	{
		$latex = 'checked';
		$latex_default = '';
	}
	else
	{
		$latex_default = 'checked';
		$latex = '';
	}

	if($tetracycline == 1)
	{
		$tetracycline = 'checked';
		$tetracycline_default = '';
	}
	else
	{
		$tetracycline_default = 'checked';
		$tetracycline = '';
	}

	if($dental_anesthetics == 1)
	{
		$dental_anesthetics = 'checked';
		$dental_anesthetics_default = '';
	}
	else
	{
		$dental_anesthetics_default = 'checked';
		$dental_anesthetics = '';
	}

	if($penicillin == 1)
	{
		$penicillin = 'checked';
		$penicillin_default = '';
	}
	else
	{
		$penicillin_default = 'checked';
		$penicillin = '';
	}


	$medical_div ='
	<h3>SCHWARZ ANAYSIS</h3>
	     <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">U1W(Equals) </label>
				                    
				                    <div class="col-md-8">
				                        <input type="text" class="form-control" name="ufouw'.$visit_id.'" id="ufouw'.$visit_id.'" placeholder="UIW" value="'.$ufouw.'">
				                    </div>
				                </div>
				            </div><div class="row">
						<div class="col-md-6">
							<div class="form-group">
			                    <label class="col-md-4 control-label"> </label>
			                    
			                    <div class="col-md-12">
			                        <div class="col-md-4">
		                                <div class="radio">
		                                    <label>      
		                                        Cast
		                                    </label>
		                                </div>
		                            </div>
		                            <div class="col-md-4">
		                                <div class="radio">
		                                    <label>
		                        
		                                     Expected
		                                    </label>
		                                </div>
		                            </div>
		                         
			                    </div>
			                </div>
			                <div class="col-md-12">
			                <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">U4</label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="ufour'.$visit_id.'" id="ufour'.$visit_id.'" placeholder="U4" value="'.$ufour.'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"></label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="ufourr'.$visit_id.'" id="ufourr'.$visit_id.'" placeholder="u4" value="'.$ufourr.'">
				                    </div>
				                </div>
				            </div>
				            </div>
				               <div class="col-md-12">
				               <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">U6 </label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="usix'.$visit_id.'" id="usix'.$visit_id.'" placeholder="U6" value="'.$usix.'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"></label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="usixx'.$visit_id.'" id="usixx'.$visit_id.'" placeholder="U6" value="'.$usixx.'">
				                    </div>
				                </div>
				            </div> 
				            </div>
                                <div class="col-md-12">
				               <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">L4</label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="lfour'.$visit_id.'" id="lfour'.$visit_id.'" placeholder="L4" value="'.$lfour.'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"> </label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="lfourr'.$visit_id.'" id="lfourr'.$visit_id.'" placeholder="L4" value="'.$lfourr.'">
				                    </div>
				                </div>
				            </div>
				          
				            </div>
				           <div class="col-md-12">
				         <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">L6</label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="lsix'.$visit_id.'" id="lsix'.$visit_id.'" placeholder="L6" value="'.$lsix.'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"> </label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="lsixx'.$visit_id.'" id="lsixx'.$visit_id.'" placeholder="L6" value="'.$lsixx.'">
				                    </div>
				                </div>
				            </div>
				       
				            </div>
				            <br/>

				            	<h3>CEPH REPORT</h3>
                              <div class="row">				            
                                <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">SNA </label>
				                    
				                    <div class="col-md-8">
				                        <input type="text" class="form-control" name="sna'.$visit_id.'" id="sna'.$visit_id.'" placeholder="SNA" value="'.$sna.'">
				                    </div>
				                </div>
				                </div>
				                  <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">SNB </label>
				                    
				                    <div class="col-md-8">
				                        <input type="text" class="form-control" name="snb'.$visit_id.'" id="snb'.$visit_id.'" placeholder="SNB" value="'.$snb.'">
				                    </div>
				                </div>
				                </div>
				                  <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">Air Way</label>
				                    
				                    <div class="col-md-8">
				                        <input type="text" class="form-control" name="air'.$visit_id.'" id="air'.$visit_id.'" placeholder="Air Way" value="'.$air.'">
				                    </div>
				                </div>
				                </div>
				                <div>
				              <div class="row">
				                   <div class="form-group">
			                    <label class="col-md-6 control-label">General Notes</label>
			                    
			                    <div class="col-md-12">
			                        <textarea class="form-control" name="none'.$visit_id.'" id="none'.$visit_id.'">'.$none.'</textarea>
			                    </div>
			                </div>
			                </div>




			               
			             
			          
			                   
			         

	                
				';
}
else
{
	$medical_div ='
					<h3>SCHWARZ ANAYSIS</h3>
	     <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">U1W(Equals) </label>
				                    
				                     <div class="col-md-6">
				                        <input type="text" class="form-control" name="ufouw'.$visit_id.'" id="ufouw'.$visit_id.'" placeholder="First name" value="'.set_value('ufouw').'">
				                    </div>
				                </div>
				            </div><div class="row">
						<div class="col-md-6">
							<div class="form-group">
			                    <label class="col-md-4 control-label"> </label>
			                    
			                   <div class="col-md-12">
			                        <div class="col-md-4">
		                                <div class="radio">
		                                    <label>      
		                                        Cast
		                                    </label>
		                                </div>
		                            </div>
		                            <div class="col-md-4">
		                                <div class="radio">
		                                    <label>
		                        
		                                     Expected
		                                    </label>
		                                </div>
		                            </div>
		                         
			                    </div>
			                </div>
			             <div class="col-md-12">
			                <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">U4</label>
				                    
				                    <div class="col-md-6">
				                        <input type="text" class="form-control" name="ufour'.$visit_id.'" id="ufour'.$visit_id.'" placeholder="First name" value="'.set_value('ufour').'">
				                    </div>
				                </div>
				            </div>


				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"></label>
				                    
				                    <div class="col-md-6">
				                       <input type="text" class="form-control" name="ufourr'.$visit_id.'" id="ufourr'.$visit_id.'" placeholder="First name" value="'.set_value('ufourr').'">
				                    </div>
				                </div>
				            </div>
				   
				            </div>
				            <div class="col-md-12">
				               <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">U6 </label>
				                    
				                    <div class="col-md-6">
				                 <input type="text" class="form-control" name="usix'.$visit_id.'" id="usix'.$visit_id.'" placeholder="First name" value="'.set_value('usix').'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"></label>
				                    
				                    <div class="col-md-6">
				                       <input type="text" class="form-control" name="usixx'.$visit_id.'" id="usixx'.$visit_id.'" placeholder="First name" value="'.set_value('usixx').'">
				                    </div>
				                </div>
				            </div>
				      
				            </div>
				            <div class="col-md-12">
				               <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">L4</label>
				                    
				                    <div class="col-md-6">
				                         <input type="text" class="form-control" name="lfour'.$visit_id.'" id="lfour'.$visit_id.'" placeholder="First name" value="'.set_value('lfour').'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"> </label>
				                    
				                    <div class="col-md-6">
				                          <input type="text" class="form-control" name="lfourr'.$visit_id.'" id="lfourr'.$visit_id.'" placeholder="First name" value="'.set_value('lfourr').'">
				                    </div>
				                </div>
				            </div>
				
				            </div>
                         <div class="col-md-12">
				         <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label">L6</label>
				                    
				                    <div class="col-md-6">
				                       <input type="text" class="form-control" name="lsix'.$visit_id.'" id="lsix'.$visit_id.'" placeholder="First name" value="'.set_value('lsix').'">
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-6">
				                <div class="form-group">
				                    <label class="col-md-2 control-label"> </label>
				                    
				                    <div class="col-md-6">
				                       <input type="text" class="form-control" name="lsixx'.$visit_id.'" id="lsixx'.$visit_id.'" placeholder="First name" value="'.set_value('lsixx').'">
				                    </div>
				                </div>
				            </div>
				            </div>
				            <br/>

				            	<h3>CEPH REPORT</h3>
                              <div class="row">				            
                                <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">SNA </label>
				                    
				                    <div class="col-md-8">
				                         <input type="text" class="form-control" name="sna'.$visit_id.'" id="sna'.$visit_id.'" placeholder="First name" value="'.set_value('sna').'">				                    </div>
				                </div>
				                </div>
				                  <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">SNB </label>
				                    
				                    <div class="col-md-8">
				                        <input type="text" class="form-control" name="snb'.$visit_id.'" id="snb'.$visit_id.'" placeholder="First name" value="'.set_value('snb').'">
				                    </div>
				                </div>
				                </div>
				                  <div class="col-md-8">
				                <div class="form-group">
				                    <label class="col-md-4 control-label">Air Way</label>
				                    
				                    <div class="col-md-8">
				                          <input type="text" class="form-control" name="air'.$visit_id.'" id="air'.$visit_id.'" placeholder="First name" value="'.set_value('air').'">
				                    </div>
				                </div>
				                </div>
				            <div class="row">
				                   <div class="form-group">
			                    <label class="col-md-6 control-label">General Notes</label>
			                    
			                    <div class="col-md-12">
			                       <textarea class="form-control" name="none'.$visit_id.'" id="none'.$visit_id.'">'.set_value('none').'</textarea>
			                    </div>
			                </div>
			                </div>
				            




				';
}




// presenting complaint 
$table ='visit_prescription';
$where ='visit_prescription_delete = 0 AND visit_id ='.$visit_id;
$select = '*';

$prescription_rs = $this->dental_model->get_items_from_table($table,$where,$select);
// var_dump($prescription_rs); die();
$prescription_div ='';
if($prescription_rs->num_rows() > 0)
{
	foreach ($prescription_rs->result() as $key_prescription) {
		# code...

		// $visit_prescription_id=$key_prescription->visit_prescription_id;
		$visit_prescription=$key_prescription->visit_prescription;
	}

	$prescription_div ='<textarea class="form-control" rows="5" name="visit_prescription'.$visit_id.'" id="visit_prescription'.$visit_id.'">'.$visit_prescription.'</textarea>';
}
else
{
	$prescription_div ='<textarea class="form-control" class="cleditor" rows="5" name="visit_prescription'.$visit_id.'" id="visit_prescription'.$visit_id.'">'.set_value('visit_prescription').'</textarea>';
}


// end of presenting complaint
?>


<div class="row" >   
<section class="panel panel-primary">

    <div class="panel-body">
        <div class="padd">
            <div class="col-md-18">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<?php echo form_open('', array('class' => 'form-horizontal', 'id' => 'medical_form', 'visit_id' => $visit_id));?>

					<?php echo $medical_div;?>

					 <div class="form-actions left-align" style="margin-top:10px;">
                            <button class="submit btn btn-primary" type="submit">
                                UPDATE
                            </button>
                        </div>
                    <?php echo form_close();?>
					
		        
		        </div>
			</div>
			
        </div>
    </div>
</section>
</div>