<div class="row">
    <div class="col-md-12">
      <!-- Widget -->
      <div class="widget boxed">
        <!-- Widget head -->
        <div class="widget-head">
          <h4 class="pull-left"><i class="icon-reorder"></i>Vitals</h4>
          <div class="widget-icons pull-right">
            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
            <a href="#" class="wclose"><i class="icon-remove"></i></a>
          </div>
          <div class="clearfix"></div>
        </div>             
            <!-- Widget content -->
            <div class="widget-content">
              <div class="padd">
                    <!-- vitals from java script -->
                    <div id="vitals"></div>
                    <!-- end of vitals data -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <!-- Widget -->
      <div class="widget boxed">
        <!-- Widget head -->
        <div class="widget-head">
          <h4 class="pull-left"><i class="icon-reorder"></i>Vitals history</h4>
          <div class="widget-icons pull-right">
            <a href="#" class="wminimize"><i class="icon-chevron-up"></i></a> 
            <a href="#" class="wclose"><i class="icon-remove"></i></a>
          </div>
          <div class="clearfix"></div>
        </div>             
            <!-- Widget content -->
            <div class="widget-content">
              <div class="padd">
                <div id="previous_vitals"></div>
            </div>
        </div>
        </div>
    </div>
</div>


<!-- <div class="row">
    <div class="col-md-12"> 
        <div class="panel">
            <div class="panel-heading  ">
                <h4 class="panel-title">Procedures</h4>
                <div class="panel-tools pull-right">
                  <input type='button' class="btn btn-sm btn-warning sidebar-right-toggle" data-open="sidebar-right" value='Procedures' data-toggle='control-sidebar' onclick='procedures_sidebar(<?php echo $visit_id;?>)'/>
              </div>
            </div>
            <div class="panel-body">    
                <div id="procedures"></div>
            </div>
         </div>
    </div>
</div> -->





<script text="javascript">
$(function() {
    $("#consumable_id").customselect();
    $("#procedure_id").customselect();
    $("#vaccine_id").customselect();
});
$(document).ready(function(){
  	vitals_interface(<?php echo $visit_id;?>);

	$(function() {
		$("#consumable_id").customselect();
		$("#vaccine_id").customselect();
		$("#procedure_id").customselect();
	});
});

 function vitals_interface(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/vitals_interface/"+visit_id;

            
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("vitals");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                obj.innerHTML = XMLHttpRequestObject.responseText;
                
                var count;
                for(count = 1; count < 12; count++){
                    load_vitals(count, visit_id);
                }
                previous_vitals(visit_id);
                // get_family_history(visit_id);
                // nurse_notes(visit_id);
                // patient_details(visit_id);
        				display_procedure(visit_id);
        				get_medication(visit_id);
        				get_surgeries(visit_id);
        				get_vaccines(visit_id);
        				display_vaccines(visit_id);
        				display_visit_vaccines(visit_id);
                display_visit_consumables(visit_id);
                get_family_diseases(visit_id);
                get_food_types_view(visit_id);
                get_patient_medicine_allergy(visit_id);
                display_nurse_notes(visit_id,17);
                display_nurse_notes(visit_id,1);
                display_nurse_notes(visit_id,18);
                
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}


function load_vitals(vitals_id, visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_vitals/"+vitals_id+"/"+visit_id;//window.alert(url);
  
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("vital"+vitals_id);
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
                
                if((vitals_id == 8) || (vitals_id == 9)){
                    calculate_bmi(visit_id);
                }
                
                if((vitals_id == 3) || (vitals_id == 4)){
                    calculate_hwr(vitals_id, visit_id);
                }
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function previous_vitals(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/previous_vitals/"+visit_id;//window.alert(url);

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("previous_vitals");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function getXMLHTTP() {
     //fuction to return the xml http object
        var xmlhttp=false;  
        try{
            xmlhttp=new XMLHttpRequest();
        }
        catch(e)    {       
            try{            
                xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch(e){
                try{
                xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch(e1){
                    xmlhttp=false;
                }
            }
        }
            
        return xmlhttp;
    }
	
    function save_vital(visit_id)
	{
        var config_url = document.getElementById("config_url").value;
        var data_url = config_url+"nurse/save_vitals/"+visit_id;
       
        var vital5_systolic = $('#vital5').val();
        var vital6_diastolic = $('#vital6').val();
        var vital8_weight = $('#vital8').val();
        var vital9_height = $('#vital9').val();
        var vital4_hip = $('#vital4').val();
        var vital3_waist = $('#vital3').val();
        var vital1_temperature = $('#vital1').val();
        var vital7_pulse = $('#vital7').val();
        var vital2_respiration = $('#vital2').val();
        var vital11_oxygen = $('#vital11').val();
        var vital10_pain = $('#vital10').val();
         
        $.ajax({
        type:'POST',
        url: data_url,
        data:{systolic: vital5_systolic, diastolic : vital6_diastolic, weight: vital8_weight, height : vital9_height,hip : vital4_hip,waist : vital3_waist, temperature : vital1_temperature,pulse : vital7_pulse,respiration: vital2_respiration,oxygen: vital11_oxygen, pain: vital10_pain},
        dataType: 'text',
        success:function(data)
		{
			//calculate_bmi(visit_id);
         //get_medication(visit_id);
         alert("You have successfully entered the vitals");
          previous_vitals(visit_id);
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });
        
    }

    function calculate_bmi(visit_id){
    
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var config_url = document.getElementById("config_url").value;
        var url = config_url+"nurse/calculate_bmi/"+visit_id;

        if(XMLHttpRequestObject) {
            
            var obj = document.getElementById("bmi_out");
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    //obj.innerHTML = XMLHttpRequestObject.responseText;
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }

function calculate_hwr(vitals_id, visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/calculate_hwr/"+vitals_id+"/"+visit_id;//window.alert(url);

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("hwr_out");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                //obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function display_procedure(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/view_procedure/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function display_bed_charges(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/view_bed_charges/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("bed_charges").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function display_consultation_charges(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/view_consultation_charges/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("consultation_charges").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function myPopup3(visit_id) {
    var config_url = document.getElementById("config_url").value;
    var win = window.open( config_url+"nurse/procedures/"+visit_id, "myWindow", "status = 1, height = auto, width = 600, resizable = 0" );
  	win.focus();
}

function calculatevaccinetotal(amount, id, procedure_id, v_id)
{
    var units = document.getElementById('units'+id).value;  

    grand_vaccine_total(id, units, amount, v_id);
}
function calculateconsumabletotal(amount, id, procedure_id, v_id)
{
    var units = document.getElementById('units'+id).value;  

    grand_consumable_total(id, units, amount, v_id);
}

function display_visit_vaccines(visit_id)
{
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/visit_vaccines/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        var obj = document.getElementById("vaccines_to_patients");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			{
                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function display_visit_consumables(visit_id)
{
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/visit_consumables/"+visit_id;
    
    if(XMLHttpRequestObject) {
                
        var obj = document.getElementById("consumables_to_patients");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
            {
                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function grand_vaccine_total(vaccine_id, units, amount, v_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/vaccine_total/"+vaccine_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                display_visit_vaccines(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function grand_consumable_total(vaccine_id, units, amount, v_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/consuamble_total/"+vaccine_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                display_visit_consumables(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

//Calculate procedure total
function calculatetotal(amount, id, procedure_id, v_id){
       
    var units = document.getElementById('units'+id).value;  

    grand_total(id, units, amount, v_id);
}

//Calculate bed total
function calculatebedtotal(amount, visit_charge_id, service_charge_id, v_id){
       
    var units = document.getElementById('bed_charge_units'+visit_charge_id).value;  

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/bed_total/"+visit_charge_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			{
    			display_bed_charges(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

//Calculate consultation total
function calculateconsultationtotal(amount, visit_charge_id, service_charge_id, v_id){
       
    var units = document.getElementById('consultation_charge_units'+visit_charge_id).value;  

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/bed_total/"+visit_charge_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			{
    			display_consultation_charges(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function grand_total(procedure_id, units, amount, v_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var url = config_url+"nurse/procedure_total/"+procedure_id+"/"+units+"/"+amount;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) 
			{
    			display_procedure(v_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_procedure(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_procedure/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_procedure(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_bed(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_bed/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_bed_charges(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_consultation(id, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
     var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_bed/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                display_bed_charges(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_vaccine(id, visit_id){
	
	var confirmation = confirm('Do you really want to delete this vaccine ?');
	
	if(confirmation)
	{
		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		 var config_url = document.getElementById("config_url").value;
		var url = config_url+"nurse/delete_vaccine/"+id;
		
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	
					display_visit_vaccines(visit_id);
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}
}
function delete_consumable(id, visit_id){
	
	var confirmation = confirm('Delete consumable?');
	
	if(confirmation)
	{
		var XMLHttpRequestObject = false;
			
		if (window.XMLHttpRequest) {
		
			XMLHttpRequestObject = new XMLHttpRequest();
		} 
			
		else if (window.ActiveXObject) {
			XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
		}
		var config_url = document.getElementById("config_url").value;
		var url = config_url+"nurse/delete_consumable/"+id;
		
		if(XMLHttpRequestObject) {
					
			XMLHttpRequestObject.open("GET", url);
					
			XMLHttpRequestObject.onreadystatechange = function(){
				
				if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
	
					display_visit_consumables(visit_id);
				}
			}
					
			XMLHttpRequestObject.send(null);
		}
	}
}




// start of vaccine

function display_vaccines(visit_id){

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/visit_vaccines/"+visit_id;
  
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                document.getElementById("vaccines_to_patients").innerHTML=XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function myPopup4(visit_id) {
    var config_url = document.getElementById("config_url").value;
    var win = window.open( config_url+"nurse/vaccines_list/"+visit_id, "myWindow", "status = 1, height = auto, width = 600, resizable = 0" );
  	win.focus();
}
function myPopup5(visit_id) {
    var config_url = document.getElementById("config_url").value;
    var win = window.open( config_url+"nurse/consumables_list/"+visit_id, "myWindow", "status = 1, height = auto, width = 600, resizable = 0" );
    win.focus();
}
// end of vaccine
function save_medication(visit_id){
    var config_url = document.getElementById("config_url").value;
    var data_url = config_url+"nurse/medication/"+visit_id;
   
     var patient_medication = $('#medication_description').val();
     var patient_medicine_allergies = $('#medicine_allergies').val();
     var patient_food_allergies = $('#food_allergies').val();
     var patient_regular_treatment = $('#regular_treatment').val();
     
    $.ajax({
    type:'POST',
    url: data_url,
    data:{medication: patient_medication,medicine_allergies: patient_medicine_allergies, food_allergies: patient_food_allergies, regular_treatment: patient_regular_treatment },
    dataType: 'text',
    success:function(data){
     get_medication(visit_id);
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

       
}

function get_medication(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }

    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_medication/"+visit_id;

    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("medication");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function get_surgeries(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/load_surgeries/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("surgeries");
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}


function save_surgery(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var date = document.getElementById("datepicker").value;
    var description = null;//document.getElementById("surgery_description").value;
    var month = document.getElementById("surgery_id").value;
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/surgeries/"+date+"/"+description+"/"+month+"/"+visit_id;
   
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_surgeries(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_surgery(id, visit_id){
    //alert(id);
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
      var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/delete_surgeries/"+id;
    
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_surgeries(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function get_vaccines(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_vaccine/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("patient_vaccine");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}
function save_vaccine(vaccine_id, value, visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/";
    
    if(value == 1){
        var yes =  document.getElementById("yes"+vaccine_id);
        
        if(yes.checked == true){
        
            url = url + "vaccine/"+vaccine_id+"/1" ;
            
        }
        else if(yes.checked == false){
        
            url = url + "vaccine/"+vaccine_id+"/2";
            
        }
    }
    
    else if(value == 0){
        var no =  document.getElementById("no"+vaccine_id);
        
        if(no.checked == false){
            url = url + "vaccine/"+vaccine_id+"/1";
            
        }
        else if(no.checked == true){
        
            url = url + "vaccine/"+vaccine_id+"/2";
            
        }
    }
    url = url + "/"+visit_id;
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                
                get_vaccines(visit_id);
            }
        }
        
        XMLHttpRequestObject.send(null);
    }
	
}

</script>

<script type="text/javascript">
    // other changes
    function parse_procedures(visit_id,suck)
    {
      var procedure_id = document.getElementById("procedure_id").value;
       procedures(procedure_id, visit_id, suck);
    }
    function parse_vaccine(visit_id,suck)
    {
      var vaccine_id = document.getElementById("vaccine_id").value;
       vaccines_anchor(vaccine_id, visit_id, suck);
      
    }
    function procedures(id, v_id, suck){
      
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        var url = "<?php echo site_url();?>nurse/procedure/"+id+"/"+v_id+"/"+suck;
       
         if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("procedures").innerHTML=XMLHttpRequestObject.responseText;
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

    }
    function vaccines_anchor(id, v_id, suck){
   
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
      
        var url = "<?php echo site_url();?>nurse/vaccines/"+id+"/"+v_id+"/"+suck;
      
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    document.getElementById("vaccines_to_patients").innerHTML=XMLHttpRequestObject.responseText;
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }
    }
    function parse_consumable_charge(visit_id,suck)
    {
      var consumable_id = document.getElementById("consumable_id").value;
       // alert(consumable_id);
      consumable(consumable_id, visit_id,suck);

    }

    function consumable(id, visit_id,suck){
        
        var XMLHttpRequestObject = false;
            
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var url = "<?php echo site_url();?>nurse/inpatient_consumables/"+id+"/"+visit_id+"/"+suck;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                    
                   document.getElementById("consumables_to_patients").innerHTML = XMLHttpRequestObject.responseText;
                   //get_surgery_table(visit_id);
                }
            }
            
            XMLHttpRequestObject.send(null);
        }
    }







    // procedures


  function procedures_sidebar(visit_id)
  {

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/procedures_sidebar/"+visit_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
     get_procedures_sidebar(visit_id);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

  }


function get_procedures_sidebar(visit_id){
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = $('#config_url').val();
    var url = config_url+"nurse/view_selected_procedures_tests/"+visit_id;
    // alert(url);
    if(XMLHttpRequestObject) {
        
       var obj3 = document.getElementById("view-list-procedures-tests");
        
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            
            obj3.innerHTML = XMLHttpRequestObject.responseText;

          }
        }
            
        XMLHttpRequestObject.send(null);
    }
}


function search_procedures_tests(visit_id)
{
  var config_url = $('#config_url').val();
  var data_url = config_url+"nurse/search_procedures_tests/"+visit_id;
  //window.alert(data_url);
  var procedures = $('#q').val();
  $.ajax({
  type:'POST',
  url: data_url,
  data:{visit_id: visit_id, procedures : procedures},
  dataType: 'text',
  success:function(data){
  //window.alert("You have successfully updated the symptoms");
  //obj.innerHTML = XMLHttpRequestObject.responseText;
   $("#searched-procedures-test").html(data);
    // alert(data);
  },
  error: function(xhr, status, error) {
  //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
  alert(error);
  }

  });
}

function add_procedures_test(service_charge_id, status, visit_id){

  var XMLHttpRequestObject = false;
    
  if (window.XMLHttpRequest) {
  
    XMLHttpRequestObject = new XMLHttpRequest();
  } 
    
  else if (window.ActiveXObject) {
    XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
  }
  var config_url = $('#config_url').val();
  var url = config_url+"nurse/add_procedures/"+service_charge_id+"/"+status+"/"+visit_id;


  if(XMLHttpRequestObject) {
    // var obj3 = window.opener.document.getElementById("visit_symptoms1");
     var obj3 = document.getElementById("view-list-procedures-tests");
        
    XMLHttpRequestObject.open("GET", url);
        
    XMLHttpRequestObject.onreadystatechange = function(){
      
      if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

        obj3.innerHTML = XMLHttpRequestObject.responseText;
         display_procedure(visit_id);
      }
    }
        
    XMLHttpRequestObject.send(null);
  }

   
}


 function get_procedures_table_view(visit_id)
 {
     var XMLHttpRequestObject = false;
         
     if (window.XMLHttpRequest) {
     
         XMLHttpRequestObject = new XMLHttpRequest();
     } 
         
     else if (window.ActiveXObject) {
         XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
     }
     var config_url = $('#config_url').val();
     var url = config_url+"nurse/procedures_table/"+visit_id;

 
     if(XMLHttpRequestObject) {
                 
         XMLHttpRequestObject.open("GET", url);
                 
         XMLHttpRequestObject.onreadystatechange = function(){
             
             if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
                 
                 document.getElementById("procedures_table").innerHTML = XMLHttpRequestObject.responseText;
             }
         }
         
         XMLHttpRequestObject.send(null);
     }
 }


 function delete_procedures_test(service_charge_id,status,visit_id)
 {

    var res = confirm('Are you sure you want to remove this test ? ');

    if(res)
    {
      var XMLHttpRequestObject = false;
        
      if (window.XMLHttpRequest) {
      
        XMLHttpRequestObject = new XMLHttpRequest();
      } 
        
      else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
      }
      var config_url = $('#config_url').val();
      var url = config_url+"nurse/delete_procedures_test/"+service_charge_id+"/"+status+"/"+visit_id;


      if(XMLHttpRequestObject) {
        // var obj3 = window.opener.document.getElementById("visit_symptoms1");
         var obj3 = document.getElementById("view-list-procedures-tests");
            
        XMLHttpRequestObject.open("GET", url);
            
        XMLHttpRequestObject.onreadystatechange = function(){
          
          if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

            obj3.innerHTML = XMLHttpRequestObject.responseText;
             display_procedure(visit_id);
          }
        }
            
        XMLHttpRequestObject.send(null);
      }
    }
 }


 function surgical_sidebar(visit_id)
 {
  open_sidebar();
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/surgical_sidebar/"+visit_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
 }

 function save_new_surgery(visit_id)
 {
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_surgery_list";
    //window.alert(data_url);
    var surgery_name = document.getElementById("surgery_name").value;


    if(surgery_name == null || surgery_name == ""){
        alert("Sorry please ensure you written a surgery name");
    }
    else
    {
        $.ajax({
        type:'POST',
        url: data_url,
        data:{surgery_name: surgery_name},
        dataType: 'text',
        success:function(data){
            alert('You have successfully added the surgery name');
            document.getElementById("surgery_name").value = '';;
            get_surgeries(visit_id);
            close_side_bar();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });

    }
    
 }


 function vaccines_sidebar(visit_id)
 {
    open_sidebar();
     var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/vaccine_sidebar/"+visit_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
 }

 function save_new_vaccine(visit_id)
 {
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_vaccine_list";
    //window.alert(data_url);
    var vaccine_name = document.getElementById("vaccine_name").value;


    if(vaccine_name == null || vaccine_name == ""){
        alert("Sorry please ensure you written a vaccine name");
    }
    else
    {
        $.ajax({
        type:'POST',
        url: data_url,
        data:{vaccine_name: vaccine_name},
        dataType: 'text',
        success:function(data){
            alert('You have successfully added the vaccine name');
            document.getElementById("vaccine_name").value = '';;
            get_vaccines(visit_id);
            close_side_bar();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });

    }
    
 }




 function family_diseases_sidebar(visit_id)
 {
    open_sidebar();
     var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/family_disease_sidebar/"+visit_id;
    //window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
 }

 function save_new_family_disease(visit_id)
 {
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_family_disease_list";
    //window.alert(data_url);
    var family_disease_name = document.getElementById("family_disease_name").value;


    if(family_disease_name == null || family_disease_name == ""){
        alert("Sorry please ensure you written a family_disease name");
    }
    else
    {
        $.ajax({
        type:'POST',
        url: data_url,
        data:{family_disease_name: family_disease_name},
        dataType: 'text',
        success:function(data){
            alert('You have successfully added the family_disease name');
            document.getElementById("family_disease_name").value = '';;
            get_family_diseases(visit_id);
            close_side_bar();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });

    }
    
 }

 function get_family_diseases(visit_id){
    
    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_family_disease/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("family-diseases");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}



    
function save_condition1(cond, family, patient_id)
{
    var config_url = $("#config_url").val();
    url = config_url+"nurse/save_family_disease/"+cond+"/"+family+"/"+patient_id;
    // alert(url);
    $.get( url, function( data ) {
        $.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
            $("#new-nav").html(data);
        });
    });
}

function delete_condition(cond, family, patient_id)
{
    var config_url = $("#config_url").val();
    url = config_url+"nurse/delete_family_disease/"+cond+"/"+family+"/"+patient_id;
    // alert(url);
    $.get( url, function( data ) {
        $.get( config_url+"nurse/get_family_history/<?php echo $visit_id;?>", function( data ) {
            $("#new-nav").html(data);
        });
    });
}
function save_patient_history(visit_id)
{
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/save_patient_dx";
    // alert(data_url);
     var patient_dx = document.getElementById("patient_dx").value;
     var date = document.getElementById("date").value;
     var time = document.getElementById("time").value;

     $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: patient_dx, date : date, time : time},
        dataType: 'text',
        success:function(data){
            // alert('You have successfully added the family_disease name');
            // document.getElementById("family_disease_name").value = '';
            // get_family_diseases(visit_id);
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

    });

}

function food_sidebar(visit_id)
{
    open_sidebar();
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/new_food_type_sidebar/"+visit_id;
    // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}
function save_new_food_type(visit_id)
{


    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_food_type_list";
    //window.alert(data_url);
    var food_name = document.getElementById("food_name").value;


    if(food_name == null || food_name == ""){
        alert("Sorry please ensure you written a food name");
    }
    else
    {
        $.ajax({
        type:'POST',
        url: data_url,
        data:{food_name: food_name},
        dataType: 'text',
        success:function(data){
            alert('You have successfully added the food name');
            document.getElementById("food_name").value = '';;
            get_food_types_view(visit_id);
            close_side_bar();
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }

        });

    }


}

function get_food_types_view(visit_id)
{

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_food_types/"+visit_id;
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("food-allergy");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function save_food_allergy(visit_id)
{


    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var serviority = document.getElementById("serviority").value;
    var reaction = document.getElementById("reaction").value;
    var food_type_id = document.getElementById("food_type_id").value;
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/food_types/"+serviority+"/"+reaction+"/"+food_type_id+"/"+visit_id;
   
    if(XMLHttpRequestObject) {
                
        XMLHttpRequestObject.open("GET", url);
                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                get_food_types_view(visit_id);
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_food_allergy(food_allergy_id,visit_id)
{

    var res = confirm('Are you sure you want to delete this entry ? ');

    if(res)
    {

         var XMLHttpRequestObject = false;
        
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
          var config_url = document.getElementById("config_url").value;
        var url = config_url+"nurse/delete_food_allergy/"+food_allergy_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    get_food_types_view(visit_id);
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

    }
   
}
function medicine_sidebar(visit_id)
{
    document.getElementById("sidebar-right").style.display = "block"; 
    document.getElementById("current-sidebar-div").style.display = "none"; 
    // alert('sS');
    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/medicine_allergy_sidebar/"+visit_id;
    // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
     $("#sidebar-div").html(data);
      // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });


}

function search_drug_list(visit_id)
{

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/search_drug_list/"+visit_id;
    //window.alert(data_url);
    var drug = $('#q').val();
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id, drug : drug},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;
    $("#searched-medicine").html(data);
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}

function add_drug_allergy(product_id, status, visit_id){

  

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/add_drug_allergy/"+product_id+"/"+status+"/"+visit_id;
    //window.alert(data_url);
    // var drug = $('#q').val();
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id},
    dataType: 'text',
    success:function(data){
         var data = jQuery.parseJSON(data);

        // alert(data.message);
      //window.alert("You have successfully updated the symptoms");
      //obj.innerHTML = XMLHttpRequestObject.responseText;
      $('#adding-list').css('display', 'block');
      document.getElementById("product_id").value = product_id;
      document.getElementById("product_name").value = data.product_name;

    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });

   
}


function save_drug_allergy(visit_id)
{

    var config_url = $('#config_url').val();
    var data_url = config_url+"nurse/save_drug_allergy/"+visit_id;
   
    var product_id = $('#product_id').val();
    var reaction = $('#reaction_medicine').val();
    var serviority = $('#serviority_medicine').val();
     // window.alert(data_url);
    $.ajax({
    type:'POST',
    url: data_url,
    data:{visit_id: visit_id, product_id : product_id, reaction: reaction, serviority: serviority},
    dataType: 'text',
    success:function(data){
    //window.alert("You have successfully updated the symptoms");
    //obj.innerHTML = XMLHttpRequestObject.responseText;

    var data = jQuery.parseJSON(data);
    if(data.status = 1)
    {
        $('#adding-list').css('display', 'none');
        document.getElementById("product_id").value = '';
        document.getElementById("product_name").value = '';
        alert('You have successfully added the allergy detail');
        get_patient_medicine_allergy(visit_id);
        close_side_bar();
    }
    else
    {
        alert('could not add the allergy details please try again');
    }
    // alert(data);
    },
    error: function(xhr, status, error) {
    //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
    alert(error);
    }

    });
}


function get_patient_medicine_allergy(visit_id)
{

    var XMLHttpRequestObject = false;
        
    if (window.XMLHttpRequest) {
    
        XMLHttpRequestObject = new XMLHttpRequest();
    } 
        
    else if (window.ActiveXObject) {
        XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    var config_url = document.getElementById("config_url").value;
    var url = config_url+"nurse/patient_medicine_allergy/"+visit_id;

    // alert(url);
    
    if(XMLHttpRequestObject) {
        
        var obj = document.getElementById("medicine-allergy");
                
        XMLHttpRequestObject.open("GET", url);

                
        XMLHttpRequestObject.onreadystatechange = function(){
            
            if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                obj.innerHTML = XMLHttpRequestObject.responseText;
            }
        }
                
        XMLHttpRequestObject.send(null);
    }
}

function delete_medicine_allergy(medicine_id,visit_id)
{
     var res = confirm('Are you sure you want to delete this entry ? ');

    if(res)
    {

         var XMLHttpRequestObject = false;
        
        if (window.XMLHttpRequest) {
        
            XMLHttpRequestObject = new XMLHttpRequest();
        } 
            
        else if (window.ActiveXObject) {
            XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
        }
          var config_url = document.getElementById("config_url").value;
        var url = config_url+"nurse/delete_medicine_allergy/"+medicine_id;
        
        if(XMLHttpRequestObject) {
                    
            XMLHttpRequestObject.open("GET", url);
                    
            XMLHttpRequestObject.onreadystatechange = function(){
                
                if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {

                    get_patient_medicine_allergy(visit_id);
                }
            }
                    
            XMLHttpRequestObject.send(null);
        }

    }
}

function display_nurse_notes(visit_id,type)
{

 var XMLHttpRequestObject = false;
     
 if (window.XMLHttpRequest) {
 
     XMLHttpRequestObject = new XMLHttpRequest();
 } 
     
 else if (window.ActiveXObject) {
     XMLHttpRequestObject = new ActiveXObject("Microsoft.XMLHTTP");
 }
 
 var config_url = document.getElementById("config_url").value;
 var url = config_url+"nurse/display_nurses_notes/"+visit_id+"/"+type;
// alert(url);
 if(XMLHttpRequestObject) {
             
     XMLHttpRequestObject.open("GET", url);
             
     XMLHttpRequestObject.onreadystatechange = function(){
         
         if (XMLHttpRequestObject.readyState == 4 && XMLHttpRequestObject.status == 200) {
            if(type == 17)
            {

             document.getElementById("pt-dx").innerHTML=XMLHttpRequestObject.responseText;
            }
            else if(type == 18)
            {
              document.getElementById("pt-rx").innerHTML=XMLHttpRequestObject.responseText;
            }
            if(type == 17)
            {

             document.getElementById("chief-complain").innerHTML=XMLHttpRequestObject.responseText;
            }
            
         }
     }
             
     XMLHttpRequestObject.send(null);
 }
}

 function save_nurses_notes(visit_id,type)
   {

      var config_url = $('#config_url').val();
      var data_url = "<?php echo site_url();?>nurse/save_nurses_notes/"+visit_id+"/"+type;
      
      
      if(type == 17)
      {
         var symptoms = document.getElementById("past_diagnosis_dx"+visit_id).value; 
      }
      else if(type == 18)
      {
         var symptoms = document.getElementById("past_treatment_rx"+visit_id).value; 
      }
     
      

      // window.alert(type);

      $.ajax({
        type:'POST',
        url: data_url,
        data:{notes: symptoms},
        dataType: 'json',
        success:function(data){
          if(data.result == 'success')
          {
            // $('#history_notes').html(data.message);
            // tinymce.get('visit_presenting_complaint').setContent('');
           
            alert("You have successfully saved the note");
            
            display_nurse_notes(visit_id,type);
          }
          else
          {
            alert("Unable to add the action plan");
          }
        //obj.innerHTML = XMLHttpRequestObject.responseText;
        },
        error: function(xhr, status, error) {
        //alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
        alert(error);
        }
      });

   }

   function get_view(class_id,patient_id,visit_id)
   {
    
   }

  
</script>